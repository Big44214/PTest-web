import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  private _userName = '';
  private _role = '';

  constructor() {}

  getUserName(): string{
    return this._userName;
  }

  setUserName(u: string): string{
    return this._userName = u;
  }

   getRole(): string{
    return this._role;
  }

  setRole(u: string): string{
    return this._role = u;
  }
}
