import { Injectable } from '@angular/core';
import * as context from '../interfaces/context-part.interface';

@Injectable()
export class RestServerService {

  constructor() { }

  public getAPI(postFix: string): string {
    return context.base_restserver_url+`${postFix}`;
  }

}
