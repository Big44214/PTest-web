//Area of Auth service.
export const http_basic: string = 'aWhyLXdlYjpzNHQyR1JUU0hXR0FSZDd6YlFteDR1SFQ=';
//export const base_authserver_url = 'https://gentle-beyond-63622.herokuapp.com';
export const base_authserver_url = 'http://localhost:8080/erp';
//export const base_authserver_url = 'http://172.16.0.43:8079';
export const csrf_token_uri: string = `${base_authserver_url}/csrf`;
export const login_uri: string = `${base_authserver_url}/login`;
export const logout_uri: string = `${base_authserver_url}/logout`;
export const ACCESS_TOKEN_KEY: string = 'id_token';
export const CSRF_TOKEN_HEADER: string = 'csrf_token_header';
export const CSRF_TOKEN: string = 'csrf_token';

//Area of rest service.
export const base_restserver_url = `http://localhost:8080/erp/`;