import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosComponent } from './pos.component';
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [{
    path: '', component: PosComponent
    , children: [
        { path: 'dashboard'
            , loadChildren: './pos-dashboard/pos-dashboard.module#PosDashboardModule'
            , canActivateChild: [AuthGuard], data: { preload: true }
        }
        , { path: 'daily'
            , loadChildren: './pos-dt/pos-dt.module#PosDtModule'
            , canActivateChild: [AuthGuard], data: { preload: true }
        }
        , { path: ''
            , redirectTo: 'dashboard'
            , pathMatch: 'full' 
        }
    ]
   }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ]
    , exports: [ RouterModule ]
})
export class PosRoutingModule { }
