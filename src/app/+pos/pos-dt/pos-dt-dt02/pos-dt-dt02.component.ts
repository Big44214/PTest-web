import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-pos-dt-dt02'
    , templateUrl: './pos-dt-dt02.component.html'
    , styleUrls: ['./pos-dt-dt02.component.scss']
})
export class PosDtDt02Component implements OnInit {

    private form: FormGroup;
    private records: any[];

    constructor(@Inject(FormBuilder) fb: FormBuilder
     , private router: Router) {
        this.form = fb.group({
            inputSearch:  fb.control("", Validators.required)
        });
     }

    ngOnInit() {
        this.onSearch();
     }

    private onSearch(){
        this.records = [{
            'sod': 'sod00001'
            , 'soddate': new Date()
            , 'Qty': 10
            , 'tamount': 5000000.06
        }];
        // console.info(this.form.valid);
        // console.info(this.form.get('inputSearch').value);
    }

      private test(rowRecord: number){
        this.router.navigate(['pos/daily/saleDetail'], { skipLocationChange: true });
    }

}
