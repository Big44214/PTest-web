import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
    selector: 'app-pos-dt-dt03a'
    , templateUrl: './pos-dt-dt03a.component.html'
    , styleUrls: ['./pos-dt-dt03a.component.scss']
})
export class PosDtDt03aComponent implements OnInit {

    private form: FormGroup;
    private records: any[];
    private model: any;

    constructor(@Inject(FormBuilder) fb: FormBuilder) {
        this.form = fb.group({
            name:  fb.control("", Validators.required),
            tel:  fb.control(""),
            citizenId:  fb.control("", Validators.required),
            discount:  fb.control("", Validators.required),
            vat:  fb.control("", Validators.required),
            vatCount:  fb.control("", Validators.required)
        });
     }

     onSearch(){
         
     }

    ngOnInit() { 
        this.records = [{
            'saleId': '001'
            , 'name': 'สร้อยคอรุ่งมณี'
            , 'weight': 15.6
            , 'qty': 1
            , 'gmc': '3,000'
            , 'total': '23,000'
        }];
    }

}
