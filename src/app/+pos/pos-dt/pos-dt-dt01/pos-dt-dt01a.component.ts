import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModalOptions, NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-pos-dt-dt01a'
    , templateUrl: './pos-dt-dt01a.component.html'
    , styleUrls: ['./pos-dt-dt01a.component.scss']
})
export class PosDtDt01aComponent implements OnInit {

    private formSell: FormGroup;
    private formBuy: FormGroup;
    private formSellModal: FormGroup;
    private formBuyModal: FormGroup;
    private formSearchModal: FormGroup;
    private formCustomerModal: FormGroup;
    private formPayment: FormGroup;
    private recordsBuy: any[];
    private recordsSell: any[];
    private recordsSearch: any[];
    private recordsReceiptMini: any[];
    private model: any;
    private closeResult: string;
    private modalOptions: NgbModalOptions = {backdrop:'static',size:'lg'}
    private modalOptionsFull: NgbModalOptions = {backdrop:'static'}
    private tab = 1;
    private a: any;

    constructor(@Inject(FormBuilder) fb: FormBuilder, private modalService: NgbModal) {
        this.formSell = fb.group({
            itemId:  fb.control("", Validators.required),
            sellNo:  fb.control("", Validators.required),
            sellTax:  fb.control(1, Validators.required)
        });

        this.formBuy = fb.group({
            buyNo:  fb.control("", Validators.required),
            buyTax:  fb.control(1, Validators.required)
        });

        this.formSellModal = fb.group({
            sellItemCd:  fb.control("", Validators.required),
            sellItemWeigth:  fb.control("", Validators.required),
            sellItemPrice:  fb.control("", Validators.required),
            sellItemMGC:  fb.control("", Validators.required),
            sellItemQty:  fb.control("", Validators.required),
            sellItemTotal:  fb.control("", Validators.required)
        });

        this.formBuyModal = fb.group({
            buyItemDesc:  fb.control("", Validators.required),
            buyItemWeigth:  fb.control("", Validators.required),
            buyItemPriceMd:  fb.control("", Validators.required),
            buyItemPriceRl:  fb.control("", Validators.required)
        });

        this.formSearchModal = fb.group({
            recpNo:  fb.control("", Validators.required)
        });

        this.formCustomerModal = fb.group({
            cusNo:  fb.control("", Validators.required),
            cusGender:  fb.control("", Validators.required),
            cusTitle:  fb.control("", Validators.required),
            cusName:  fb.control("", Validators.required),
            cusSurName:  fb.control("", Validators.required),
            cusAddress:  fb.control("", Validators.required),
            cusTel:  fb.control("", Validators.required),
            cusEmail:  fb.control("", Validators.required)
        });

        this.formPayment = fb.group({
            payMeth:  fb.control("", Validators.required),
            creditNo:  fb.control("", Validators.required),
            creditName:  fb.control("", Validators.required),
            creditExp:  fb.control("", Validators.required),
            creditCV:  fb.control("", Validators.required)
        });

        // this.genReport();
     }

    ngOnInit() { 
        this.recordsSell = [{
            'purchaseId': 'A003'
            , 'detail': 'ทองแท่ง ปั๊มโลโก้ 96.5% (บรรจุในการ์ด) 1 บาท'
            , 'weigth': '15.244'
            , 'price': '21,150.00'
            , 'gmc': '100.00'
            , 'quantity': 2
            , 'total': '42,500.00'
        },
        {
            'purchaseId': 'A004'
            , 'detail': 'ทองแท่ง ปั๊มโลโก้ 96.5% (บรรจุในการ์ด) 5 บาท'
            , 'weigth': '76.220'
            , 'price': '100,750.00'
            , 'gmc': '500.00'
            , 'quantity': 1
            , 'total': '101,250.00'
        }];

        this.recordsBuy = [{
            'type': 'ทองคำแท่ง'
            , 'detail': 'ทองแท่ง ปั๊มโลโก้ 96.5%'
            , 'weigth': '20.30'
            , 'priceMd': '20,100.00'
            , 'priceRl': '19,500.00'
        },
        {
            'type': 'ทองรูปพรรณ'
            , 'detail': 'สร้อยคอลายดอกพิกุล'
            , 'weigth': '100.00'
            , 'priceMd': '50,000.00'
            , 'priceRl': '49,500.00'
        },
        {
            'type': 'ทองรูปพรรณ'
            , 'detail': 'กำไลทองคำ'
            , 'weigth': '120.00'
            , 'priceMd': '75,000.00'
            , 'priceRl': '74,650.00'
        }];

        this.recordsSearch = [{
            'no': '60100000001'
            , 'date': '12/05/2017'
            , 'seller': 'สมศักดิ์ ดีจัง'
        }];

        this.recordsReceiptMini = [{
            'no': '1'
            , 'name': 'ทองแท่ง ปั๊มโลโก้ 96.5% 1 บาท'
            , 'weigth': '15.244'
            , 'price': '21,150.00'
            , 'gmc': '100'
            , 'qty': '2'
            , 'total': '42,500.00'
        },
        {
            'no': '2'
            , 'name': 'ทองแท่ง ปั๊มโลโก้ 96.5% 5 บาท'
            , 'qty': '1'
            , 'weigth': '76.220'
            , 'price': '100,750.00'
            , 'gmc': '500'
            , 'total': '101,250.00'
        }];
    }

    private save(){
        console.info("save");
    }

    private open(content) {
    this.modalService.open(content,this.modalOptions);
    }

    private openFull(content) {
    this.modalService.open(content,this.modalOptionsFull);
    }

    private tabs(tabs) {
        if(tabs == 'sell'){
            var sellDiv = document.getElementById('sellDiv');
            sellDiv.style.display = 'block';
            var buyDiv = document.getElementById('buyDiv');
            buyDiv.style.display = 'none';

            var tabSell = document.getElementById('tabSell');
            tabSell.classList.add("active");
            var tabBuy = document.getElementById('tabBuy');
            tabBuy.classList.remove("active");
        }
        else if(tabs == 'buy'){
            var sellDiv = document.getElementById('sellDiv');
            sellDiv.style.display = 'none';
            var buyDiv = document.getElementById('buyDiv');
            buyDiv.style.display = 'block';

            var tabSell = document.getElementById('tabSell');
            tabSell.classList.remove("active");
            var tabBuy = document.getElementById('tabBuy');
            tabBuy.classList.add("active");
        }
        else if(tabs == 'receiptMini'){
            var receiptMini = document.getElementById('receiptMini');
            receiptMini.style.display = 'block';
            var receiptFull = document.getElementById('receiptFull');
            receiptFull.style.display = 'none';

            var receiptMinitab = document.getElementById('receiptMinitab');
            receiptMinitab.classList.add("active");
            var receiptFulltab = document.getElementById('receiptFulltab');
            receiptFulltab.classList.remove("active");
        }
        else if(tabs == 'receiptFull'){
            var receiptMini = document.getElementById('receiptMini');
            receiptMini.style.display = 'none';
            var receiptFull = document.getElementById('receiptFull');
            receiptFull.style.display = 'block';

            var receiptMinitab = document.getElementById('receiptMinitab');
            receiptMinitab.classList.remove("active");
            var receiptFulltab = document.getElementById('receiptFulltab');
            receiptFulltab.classList.add("active");
        }
    }

    private print(){
        var receiptMini = document.getElementById('receiptMini');
        var receiptFull = document.getElementById('receiptFull');
            // receiptMini.classList.add("printAble");
            if(receiptMini.style.display=='none'){
                receiptMini.style.display = 'block';
                receiptFull.style.display = 'none';
                window.print();
                receiptMini.style.display = 'none';
                receiptFull.style.display = 'block';
            }
            else{
                window.print();
            }
            // receiptMini.classList.remove("printAble");
    }

    private printFull(){
        var receiptFull = document.getElementById('receiptFull');
        var receiptMini = document.getElementById('receiptMini');
            receiptMini.classList.remove("printAble");
            receiptFull.classList.add("printAble");
            if(receiptFull.style.display=='none'){
                receiptFull.style.display = 'block';
                receiptMini.style.display = 'none';
                window.print();
            }
            else{
                window.print();
            }
            receiptFull.classList.remove("printAble");
            receiptMini.classList.add("printAble");
        
    }
}
