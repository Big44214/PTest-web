import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { PosDtDt01Service } from './pos-dt-dt01.service';

@Component({
    selector: 'app-pos-dt-dt01'
    , templateUrl: './pos-dt-dt01.component.html'
    , styleUrls: ['./pos-dt-dt01.component.scss']
    , providers: [PosDtDt01Service]
})
export class PosDtDt01Component implements OnInit {

    private form: FormGroup;
    private records: any[];

    constructor(@Inject(FormBuilder) fb: FormBuilder, private router: Router
    , private service: PosDtDt01Service) {
        this.form = fb.group({
            inputSearch:  fb.control("", Validators.required)
        });
     }

    ngOnInit() { }

    private onSearch(){
       this.service.onSearch();
    }

    private test(rowRecord: number){
        this.router.navigate(['pos/daily/purchaseDetail'], { skipLocationChange: true });
    }

}
