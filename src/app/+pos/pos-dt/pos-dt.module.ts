import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PosDtComponent } from './pos-dt.component';
import { PosDtRoutingModule } from './pos-dt-routing.module';
import { PosDtDashboardComponent } from './pos-dt-dashboard/pos-dt-dashboard.component';
import { PosDtDt01Component } from './pos-dt-dt01/pos-dt-dt01.component';
import { PosDtDt01aComponent } from './pos-dt-dt01/pos-dt-dt01a.component';
import { PosDtDt02Component } from './pos-dt-dt02/pos-dt-dt02.component';
import { PosDtDt02aComponent } from './pos-dt-dt02/pos-dt-dt02a.component';
import { PosDtDt03Component } from './pos-dt-dt03/pos-dt-dt03.component';
import { PosDtDt03aComponent } from './pos-dt-dt03/pos-dt-dt03a.component';

export const _IMPORTS = [
    SharedModule, PosDtRoutingModule
]

export const _DECLARATIONS = [
    PosDtComponent, PosDtDashboardComponent, PosDtDt01Component
    , PosDtDt01aComponent, PosDtDt02Component, PosDtDt02aComponent
    , PosDtDt03Component, PosDtDt03aComponent
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class PosDtModule { }
