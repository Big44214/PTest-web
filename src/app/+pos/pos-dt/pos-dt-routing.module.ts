import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosDtComponent } from './pos-dt.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

import { PosDtDashboardComponent } from './pos-dt-dashboard/pos-dt-dashboard.component';
import { PosDtDt01Component } from './pos-dt-dt01/pos-dt-dt01.component';
import { PosDtDt01aComponent } from './pos-dt-dt01/pos-dt-dt01a.component';
import { PosDtDt02Component } from './pos-dt-dt02/pos-dt-dt02.component';
import { PosDtDt02aComponent } from './pos-dt-dt02/pos-dt-dt02a.component';
import { PosDtDt03Component } from './pos-dt-dt03/pos-dt-dt03.component';
import { PosDtDt03aComponent } from './pos-dt-dt03/pos-dt-dt03a.component';

const routes: Routes = [{
    path: '', component: PosDtComponent,
    children: [
        // { path: 'dashboard', component: PosDtDashboardComponent, canActivateChild: [AuthGuard] }        
        //, { path: 'purchase', component: PosDtDt01Component, canActivateChild: [AuthGuard] }
         { path: 'purchaseDetail', component: PosDtDt01aComponent, canActivateChild: [AuthGuard] }
        // , { path: 'sale', component: PosDtDt02Component, canActivateChild: [AuthGuard] }
        // , { path: 'saleDetail', component: PosDtDt02aComponent, canActivateChild: [AuthGuard] }
        // , { path: 'tranfer', component: PosDtDt03Component, canActivateChild: [AuthGuard] }
        // , { path: 'tranferDetail', component: PosDtDt03aComponent, canActivateChild: [AuthGuard] }
        , { path: '', redirectTo: 'purchaseDetail', pathMatch: 'full' }
    ]
   }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ]
    , exports: [ RouterModule ]
})
export class PosDtRoutingModule { }
