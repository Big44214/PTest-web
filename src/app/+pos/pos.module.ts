import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PosComponent } from './pos.component';
import { PosRoutingModule } from './pos-routing.module';

@NgModule({
    imports: [
        SharedModule, PosRoutingModule
    ]
    , declarations: [
        PosComponent
    ]
})
export class PosModule { }
