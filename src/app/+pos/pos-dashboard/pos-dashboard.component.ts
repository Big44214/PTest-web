import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'app-pos-dashboard'
    , templateUrl: './pos-dashboard.component.html'
    , styleUrls: ['./pos-dashboard.component.scss']
})
export class PosDashboardComponent implements OnInit {
    private records: any[];

    public brandPrimary = '#20a8d8';
    public brandSuccess = '#4dbd74';
    public brandInfo = '#63c2de';
    public brandWarning = '#f8cb00';
    public brandDanger = '#f86c6b';

    constructor(private translateService: TranslateService) {}
    ngOnInit() {
        this.records = [{
            'name': 'ทองคำแท่ง'
            , 'priceBuy': '20,050.00'
            , 'priceSell': '20,150.00'
        },
        {
            'name': 'ทองรูปพรรณ'
            , 'priceBuy': '19,692.84'
            , 'priceSell': '20,650.00'
        }];
    }

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;


    public barChartData: any[] = [
        {data: [8100, 8500, 9000, 9500, 9300, 9600, 9700], label: 'ซื้อ'}
        , {data: [8100, 8600, 9100, 9400, 9600, 9800, 10000], label: 'ขาย'}
        , {data: [7900, 8600, 9000, 9500, 9300, 9600, 9700], label: 'แลกเปลี่ยน'}
    ];
    // Doughnut
    public doughnutChartLabels: string[] = ['นำเข้า', 'นำออก', 'แลกเปลี่ยน'];
    public doughnutChartData: number[] = [350, 450, 100];
    public doughnutChartType: string = 'doughnut';
  
    public lineChartColors: Array<any> = [
    { // grey
        backgroundColor: 'rgba(148,159,177,0.2)'
        , borderColor: 'rgba(148,159,177,1)'
        , pointBackgroundColor: 'rgba(148,159,177,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)'
        , borderColor: 'rgba(77,83,96,1)'
        , pointBackgroundColor: 'rgba(77,83,96,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
        backgroundColor: 'rgba(148,159,177,0.2)'
        , borderColor: 'rgba(148,159,177,1)'
        , pointBackgroundColor: 'rgba(148,159,177,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }

    public randomize(): void {
        // Only Change 3 values
        const data = [
            getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)    
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
        ];
        const data2 = [
            getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)    
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
        ];
        const data3 = [
            getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)    
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
            , getRandomArbitrary(8000,10000)
        ];
        const clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        clone[1].data = data2;
        clone[2].data = data3;
        this.barChartData = clone;
        /**
        * (My guess), for Angular to recognize the change in the dataset
        * it has to change the dataset variable directly,
        * so one way around it, is to clone the data, change it and then
        * assign it;
        */
    }

    // lineChart
  public lineChartData: Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
  ];
  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: any = {
    animation: false,
    responsive: true
  };
  public lineChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: this.convertHex(this.brandInfo, 10),
      borderColor: this.brandInfo,
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: this.brandSuccess,
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: this.brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];

   // convert Hex to RGBA
  public convertHex(hex: string, opacity: number) {
    hex = hex.replace('#', '');
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    const rgba = 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity / 100 + ')';
    return rgba;
  }

}




function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}