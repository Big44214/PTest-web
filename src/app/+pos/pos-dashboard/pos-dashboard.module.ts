import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PosDashboardRoutingModule } from './pos-dashboard-routing.module';
import { PosDashboardComponent } from './pos-dashboard.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule, PosDashboardRoutingModule, Ng2Charts
    ],
    declarations: [
        PosDashboardComponent
    ]
})
export class PosDashboardModule { }
