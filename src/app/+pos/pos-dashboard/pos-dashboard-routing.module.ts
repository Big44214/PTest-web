import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PosDashboardComponent } from './pos-dashboard.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
    { path: '', component: PosDashboardComponent
      , canActivateChild: [AuthGuard], data: { preload: true }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
  , exports: [RouterModule]
})
export class PosDashboardRoutingModule { }
