import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

// Import BlockUI decorator & optional NgBlockUI type
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { BlockTemplateComponent } from './shared/components/block-template/block-template.component';

@Component({
  selector: 'body'
  , templateUrl: './app.component.html'
  , styleUrls: ['./app.component.css']
})
export class AppComponent {
  // Decorator wires up blockUI instance
  @BlockUI() blockUI: NgBlockUI;
  blockTemplate = BlockTemplateComponent;

  public projectname = 'projectname';

  constructor(private translate: TranslateService) {
    translate.addLangs(['en', 'th']);
    translate.setDefaultLang('th');

    this.blockUI.start(); // Start blocking
    this.blockUI.stop(); // Stop blocking


    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|th/) ? browserLang : 'th');
  }

}
