import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'app-mfu-dashboard'
    , templateUrl: './mfu-dashboard.component.html'
    , styleUrls: ['./mfu-dashboard.component.scss']
})
export class MfuDashboardComponent implements OnInit {
    constructor(private translateService: TranslateService) {}

    ngOnInit() {}

}
