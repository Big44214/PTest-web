export class Mfu01Criteria {  
    public studentId: number;
    public studentCode: string;
    public studentName: string;
    public studentAddress: string;
    public studentPhone: string;
    public studentEmail: string;
}

export class Mfu01SaveStudent {
    public studentId: number;
    public studentCode: string;
    public studentName: string;
    public studentAddress: string;
    public studentPhone: string;
    public studentEmail: string;
}