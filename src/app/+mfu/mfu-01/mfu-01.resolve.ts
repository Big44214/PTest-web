import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Mfu01Service } from './mfu-01.service';
import { BeanUtils } from '../../core/bean-utils.service';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class Mfu01Resolve implements Resolve<any> {

    constructor(private Mfu01Service: Mfu01Service) { }

    resolve(route: ActivatedRouteSnapshot): any {
        console.log(route.url[0].path);
        if (route.url[0].path == 'mfu-01') {
            return {

            };
        } else if (route.url[0].path == 'mfu-01a') {
            if (BeanUtils.isNotEmpty(route.params['studentId'])) {
                var searchData = this.Mfu01Service.searchData(route.params['studentId']);
                return Observable.forkJoin([searchData]).map((response) => {
                    return {
                        searchData: response[0]
                    };
                })
            } else {


                return {

                };

            }
        }

    };
}
