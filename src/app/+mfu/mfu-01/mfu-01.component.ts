import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mfu01Service } from './mfu-01.service';
import { Mfu01Criteria, Mfu01SaveStudent } from '../mfu-01/mfu-01.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-mfu-01',
  templateUrl: ('./mfu-01.component.html'),
  providers: [Mfu01Service]
})
export class Mfu01Component {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;


  private form: FormGroup = this.fb.group({
    studentId: this.fb.control(null)
    , studentCode: this.fb.control(null)
    , studentName: this.fb.control("")
    , studentPhone: this.fb.control(null)
    , studentAddress: this.fb.control(null)
    , studentEmail: this.fb.control(null)
  });

  // private studentId: number = null;
  private studentCode: string = "";
  private studentName: string = "";
  private studentAddress: string = "";
  private studentPhone: string = "";
  private studentEmail: string = "";


  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Mfu01SaveStudent = new Mfu01SaveStudent();
  private objDelete: Mfu01SaveStudent = new Mfu01SaveStudent();
  private alert = { title: '', msg: '' };
  private records: any[];
  private studentId: any;
  
 

  constructor(private fb: FormBuilder
    , private service: Mfu01Service
    , private router: Router
    , private routerActive: ActivatedRoute
  ) {

  }

  public childModalShow(record: any) {
    this.router.navigate(['/mfu/mfu-01/mfu-01a', record.studentId], { skipLocationChange: true });
  }

  public createStudent() {
    let studentId: any = "";
    this.router.navigate(['/mfu/mfu-01/mfu-01a', studentId], { skipLocationChange: true });
  }

  public onSave() {
    if (this.form.valid == true) {
      this.objCriteria.studentCode = this.form.controls["studentCode"].value;
      this.objCriteria.studentName = this.form.controls["studentName"].value;
      this.objCriteria.studentAddress = this.form.controls["studentAddress"].value;
      console.log(this.objCriteria);
      this.service.saveStudent(this.objCriteria).subscribe(response => {
      alert("Delete success.");
      this.records = response.records.data.studentId;
      this.studentId = response.records.data.studentId;
      
      }, );
    }
  }

  private onSearch() {
    console.log(this.objCriteria);
      this.service.searchStudent(this.objCriteria.studentId).subscribe(response => {
         this.records = response.records;
      },);
        }

   public onBeforeDelete(record: any) {
    // this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่');
    this.studentId = record.studentId
     this.onDelete();
    }

   public onDelete() {
    this.objDelete.studentId = this.studentId
    this.service.deleteStudent( this.objDelete).subscribe(response => {
      alert("Delete success.");
    this.onSearch();
   
    },
      
    );
  }

      
  }
  
  

  //   private setData(Object: any) {
  //     if (BeanUtils.isNotEmpty(Object)) {
  //       this.form.controls["keepStatusCode"].setValue(Object.records.data.keepStatusCode);
  //       this.form.controls["keepStatusName"].setValue(Object.records.data.keepStatusName);
  //       this.form.controls["keepStatusStatus"].setValue(Object.records.data.status);
  //       this.keepStatusId = Object.records.data.keepStatusId
  //     }
  //   }

  //   public onBack() {
  //     if (this.form.dirty) {
  //       this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
  //     } else {
  //       this.router.navigate(['/pm/sipmrt07'], { skipLocationChange: true });
  //     }
  //   }

  //   private backModalClose(): void {
  //     this.backModal.hide();
  //   }

  //   private backModalBack(): void {
  //     this.router.navigate(['/pm/sipmrt07'], { skipLocationChange: true });
  //   }

  //   private backModalToggle(title: string, msg: string): void {
  //     this.alert.title = title;
  //     this.alert.msg = msg;

  //     this.backModal.toggle();
  //   }

  //   public onCancel() {
  //     this.closeEvent.emit();
  //   }

  //   private changCheckbox(event: any) {
  //     if (event.target.checked) {
  //       this.form.controls["keepStatusStatus"].setValue(1);
  //     } else {
  //       this.form.controls["keepStatusStatus"].setValue(0);
  //     }

  //   }































