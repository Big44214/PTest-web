import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Mfu01Component } from './mfu-01.component';
import { Mfu01RoutingModule } from './mfu-01-routing.module';

// //Rt01.

import { Mfu01aComponent } from './mfu-01a.component';



export const _IMPORTS = [
    SharedModule, Mfu01RoutingModule
]

export const _DECLARATIONS = [
    Mfu01Component, Mfu01aComponent
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class Mfu01Module { }
