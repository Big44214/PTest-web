
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mfu01Service } from './mfu-01.service';
import { Mfu01Criteria, Mfu01SaveStudent } from './mfu-01.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';


@Component({
  selector: 'app-mfu-01a',
  templateUrl: ('./mfu-01a.component.html'),
  providers: [Mfu01Service]
})
export class Mfu01aComponent {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;


  private form: FormGroup = this.fb.group({
    studentId: this.fb.control(null)
    , studentCode: this.fb.control(null)
    , studentName: this.fb.control("")
    , studentPhone: this.fb.control(null)
    , studentAddress: this.fb.control(null)
    , studentEmail: this.fb.control(null)
  });

  private studentId: number = null;
  private studentCode: string = "";
  private studentName: string = "";
  private studentAddress: string = "";
  private studentPhone: string = "";
  private studentEmail: string = "";


  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: Mfu01SaveStudent = new Mfu01SaveStudent();
  private alert = { title: '', msg: '' };
  private records: any[];

  constructor(private fb: FormBuilder
    , private service: Mfu01Service
    , private router: Router
    , private routerActive: ActivatedRoute
  )
  {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
      console.log(this.fromLoadObject);
      this.setData(this.fromLoadObject.searchData);
    }

    ngAfterViewInit() {
      if (BeanUtils.isEmpty(this.studentId)) {
        // this.form.controls["keepStatusStatus"].setValue(1);
      }
    }

  public onSave() {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(this.studentId)) {
          this.objCriteria.studentId = this.studentId;
        }
      this.objCriteria.studentCode = this.form.controls["studentCode"].value;
      this.objCriteria.studentName = this.form.controls["studentName"].value;
      this.objCriteria.studentPhone = this.form.controls["studentPhone"].value;
      this.objCriteria.studentAddress = this.form.controls["studentAddress"].value;
      this.objCriteria.studentEmail = this.form.controls["studentEmail"].value;
      this.showMsg = false;
      console.log(this.objCriteria);
      this.service.saveStudent(this.objCriteria).subscribe(response => {
        this.records = response.records.data.studentId;
        this.studentId = response.records.data.studentId;
      }, );
    }
  }

   private setData(Object: any) {
     if (BeanUtils.isNotEmpty(Object)) {
      this.form.controls["studentCode"].setValue(Object.records.data.studentCode);
      this.form.controls["studentName"].setValue(Object.records.data.studentName);
      this.form.controls["studentPhone"].setValue(Object.records.data.studentPhone);
      this.form.controls["studentAddress"].setValue(Object.records.data.studentAddress);
      this.form.controls["studentEmail"].setValue(Object.records.data.studentEmail);
      this.studentId = Object.records.data.studentId
     }
  }


  //   private setData(Object: any) {
  //     if (BeanUtils.isNotEmpty(Object)) {
  //       this.form.controls["keepStatusCode"].setValue(Object.records.data.keepStatusCode);
  //       this.form.controls["keepStatusName"].setValue(Object.records.data.keepStatusName);
  //       this.form.controls["keepStatusStatus"].setValue(Object.records.data.status);
  //       this.keepStatusId = Object.records.data.keepStatusId
  //     }
  //   }

  //   public onBack() {
  //     if (this.form.dirty) {
  //       this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?');
  //     } else {
  //       this.router.navigate(['/pm/sipmrt07'], { skipLocationChange: true });
  //     }
  //   }

  //   private backModalClose(): void {
  //     this.backModal.hide();
  //   }

  //   private backModalBack(): void {
  //     this.router.navigate(['/pm/sipmrt07'], { skipLocationChange: true });
  //   }

  //   private backModalToggle(title: string, msg: string): void {
  //     this.alert.title = title;
  //     this.alert.msg = msg;

  //     this.backModal.toggle();
  //   }

  //   public onCancel() {
  //     this.closeEvent.emit();
  //   }

  //   private changCheckbox(event: any) {
  //     if (event.target.checked) {
  //       this.form.controls["keepStatusStatus"].setValue(1);
  //     } else {
  //       this.form.controls["keepStatusStatus"].setValue(0);
  //     }

  //   }
}






























