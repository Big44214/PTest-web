import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { RestServerService } from '../../core/services/rest-server.service';
import { Mfu01Criteria, Mfu01SaveStudent } from './mfu-01.interface';;


@Injectable()
export class Mfu01Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) {}

//saveStudent
    public saveStudent(params: Mfu01SaveStudent): Observable<any> {
      console.log(9999);
      return this.authHttp.post(this.restServer.getAPI('mfu/mfu-rt01/saveStudent'), params)
        .map(val => {
          return {
            records: val.json()
          }
        });
    }

    //Sipmrt15 search
  public searchStudent(studentID: number): Observable<any> {
    console.log(88888);
    let params = '?'+'&studentID='+studentID;
    return this.authHttp.get(this.restServer.getAPI('/mfu/mfu-rt01/searchStudent') + params)
      .map(val => {
        return {
          records: val.json().data
        }
      });
  }

   public searchData(studentId: number): Observable<any> {
     console.log(222222);
   let params = '?studentId=' + studentId;
    return this.authHttp.get(this.restServer.getAPI('/mfu/mfu-rt01/selectStudent') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }


  //deleteReasignReason
  public deleteStudent( params: Mfu01SaveStudent): Observable<any> {
    console.log(22222);
    return this.authHttp.post(this.restServer.getAPI('/mfu/mfu-rt01/deleteStudent'), params)
      .map(val => {
        return {
           records: val.json()
        }
      });
  }

    
}
