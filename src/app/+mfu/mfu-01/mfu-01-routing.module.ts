import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/guard/auth.guard';

//Component.
import { Mfu01Component } from './mfu-01.component';
import { Mfu01aComponent } from './mfu-01a.component';


// //Resolve.
import { Mfu01Resolve } from './mfu-01.resolve';


// //Service.
import { Mfu01Service } from './mfu-01.service';


const routes: Routes = [{
    path: '',
    children: [{
        path: 'mfu-01/:refresh'
        , component: Mfu01Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: Mfu01Resolve }
    },{
        path: 'mfu-01a/:studentId'
        , component: Mfu01aComponent
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: Mfu01Resolve }
    },{
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [Mfu01Service,Mfu01Resolve]
})
export class Mfu01RoutingModule { }
