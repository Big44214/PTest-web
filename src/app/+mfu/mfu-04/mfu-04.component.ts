
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mfu04Service } from './mfu-04.service';
import { SelectItem } from 'primeng/primeng';
import {tranDetail } from './mfu-04.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-mfu-04',
  templateUrl: ('./mfu-04.component.html'),
  providers: [Mfu04Service]
})
export class Mfu04Component implements OnInit{

   private form: FormGroup;
    private docType: SelectItem[];
    private transStatus: SelectItem[];
    private warehouse: SelectItem[];
    private customer: SelectItem[];
    private product: SelectItem[];
    private tranDetail: tranDetail[];
    private newTranDetail: tranDetail = new newTranDetail();


    constructor(@Inject(FormBuilder) fb: FormBuilder
        , private router: Router
        , private service: Mfu04Service
    ) {
        this.form = fb.group({
            typeItem:  fb.control("", Validators.required)
        });
    }

    ngOnInit() {
        this.docType = [];
        this.docType.push({ label: 'รายการรับสินค้า', value: 1 });
        this.docType.push({ label: 'รายการจ่ายสินค้า', value: 2 });
        this.docType.push({ label: 'รายการปรับปรุงสินค้า', value: 3 });

        this.transStatus = [];
        this.transStatus.push({ label: 'ปกติ', value: 1 });
        this.transStatus.push({ label: 'ยกเลิก', value: 0 });

        this.warehouse = [];
        this.warehouse.push({ label: 'กรุงเทพฯ', value: 1 });
        this.warehouse.push({ label: 'เชียงราย', value: 2 });

        this.customer = [];
        this.customer.push({ label: 'นาย อ้น', value: 1 });
        this.customer.push({ label: 'นาย ก่อน', value: 2 });

        this.product = [];
        this.product.push({ label: 'ทองแท่ง', value: 'ทองแท่ง' });
        this.product.push({ label: 'สร้อยคอทองคำ', value: 'สร้อยคอทองคำ' });

        this.tranDetail = [
        ];
    }

     addRow() {
        this.newTranDetail = new newTranDetail();
        this.newTranDetail.productId = 'กรุณากรอกรหัสนักศึกษา';
        let tranDetail = [...this.tranDetail];
        tranDetail.push(this.newTranDetail);
        this.tranDetail = tranDetail;
    }

    deleteRow(index) {
        console.info(index);
        this.tranDetail = this.tranDetail.filter((val,i) => i!=index);
        this.newTranDetail = null;
    }


    
    eventChangeTypeItem(){
        console.log(this.form.controls["typeItem"].value);
    }


}

class newTranDetail implements tranDetail {
    constructor(public transdtlId?, public statusDtl?, public productId?, public productDetail?
    , public productUms?, public transQty?, public transCost?, public transAmount?) {}
}






























