import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mfu04Component } from './mfu-04.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

// //Resolve.
import { Mfu04Resolve } from './mfu-04.resolve';


// //Service.
import { Mfu04Service } from './mfu-04.service';


const routes: Routes = [{
    path: '', 
    children: [{
        path: 'mfu-04/:refresh'
        , component: Mfu04Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: Mfu04Resolve }
    },{
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [Mfu04Service,Mfu04Resolve]
})
export class Mfu04RoutingModule { }
