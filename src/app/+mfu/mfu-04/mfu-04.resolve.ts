import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Mfu04Service } from './mfu-04.service';

@Injectable()
export class Mfu04Resolve implements Resolve<any> {

    constructor(private Mfu04Service: Mfu04Service) { }

    resolve(route: ActivatedRouteSnapshot): any {
        console.log(route.url[0].path);
        if (route.url[0].path == 'mfu-04') {
            return {

            };
        } 
    }
}