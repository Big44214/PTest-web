import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Mfu04Component } from './mfu-04.component';
import { Mfu04RoutingModule } from './mfu-04-routing.module';


export const _IMPORTS = [
    SharedModule, Mfu04RoutingModule
]

export const _DECLARATIONS = [
    Mfu04Component,Mfu04Component
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class Mfu04Module { }
