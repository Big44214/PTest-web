import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';
import { MfuComponent } from './mfu.component';
import { MfuDashboardComponent } from "./mfu-dashboard/mfu-dashboard.component";


const routes: Routes = [{
    path: '',
    component: MfuComponent,
    children: [
        {
            path: 'dashboard'
            , component: MfuDashboardComponent
            , canActivateChild: [AuthGuard]

        }, {
            path: 'mfu-02'
            , loadChildren: './mfu-02/mfu-02.module#Mfu02Module'
            , canActivateChild: [AuthGuard]
        }, {
            path: 'mfu-01'
            , loadChildren: './mfu-01/mfu-01.module#Mfu01Module'
            , canActivateChild: [AuthGuard]
        }, {

            path: 'mfu-03'
            , loadChildren: './mfu-03/mfu-03.module#Mfu03Module'
            , canActivateChild: [AuthGuard]
        }, {
            path: 'mfu-04'
            , loadChildren: './mfu-04/mfu-04.module#Mfu04Module'
            , canActivateChild: [AuthGuard]
        }, {
            path: 'master'
            , loadChildren: './mfu-04/mfu-04.module#Mfu04Module'
            , canActivateChild: [AuthGuard]
        }, {
            path: ''
            , redirectTo: 'dashboard'
            , pathMatch: 'full'
        }
    ]
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
})
export class MfuRoutingModule { }
