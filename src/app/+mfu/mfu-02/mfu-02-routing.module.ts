import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/guard/auth.guard';

//Component.
import { MfuRt02Component } from './mfu-02.component';
import { MfuRt02aComponent } from './mfu-02a.component';

// //Resolve.
import { MfuRt02Resolve } from './mfu-02.resolve';

// //Service.
import { MfuRt02Service } from './mfu-02.service';

const routes: Routes = [{
    path: '',
    children: [{
        path: 'mfu-02/:refresh',
        data: { title: 'MFU-02' },
        component: MfuRt02Component,
        canActivateChild: [AuthGuard],
        resolve: {fromLoadObject: MfuRt02Resolve}
    }, {        
        path: 'mfu-02a/:courseId',
        data: { title: 'MFU-02A' },
        component: MfuRt02aComponent,
        canActivateChild: [AuthGuard],
        resolve: {fromLoadObject: MfuRt02Resolve}
    }, {        
        path: 'mfu-02A/:courseId',
        component: MfuRt02aComponent,
        canActivateChild: [AuthGuard],
        resolve: {fromLoadObject: MfuRt02Resolve}
    }, { 
    path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }, 

    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [MfuRt02Service, MfuRt02Resolve]
})
export class Mfu02RoutingModule { }
