import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Page } from '../../shared/pagination.component';
import { RestServerService } from '../../core/services/rest-server.service';
import { MfuRt02Criteria, MfuRt02SaveCourse } from './mfu-02.interface';;
import { BeanUtils } from '../../core/bean-utils.service';

@Injectable()
export class MfuRt02Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //saveCourse
  public saveCourse(params: MfuRt02SaveCourse): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('mfu/mfu-rt02/saveCourse'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //searchCourse
  public searchCourse(page: Page, searchValue: MfuRt02Criteria): Observable<any> {
    let params = '?' + '&page='+ page + '&inputSearch=' + searchValue.inputSearch;
    return this.authHttp.get(this.restServer.getAPI('/mfu/mfu-rt02/searchCourse') + params)
      .map(val => {
        return { 
          page: {
            total: val.json().total,
            start: page.start,
            limit: page.limit
          }
          , records: val.json().data
        }
      });
  }

  //deleteCourse
  public deleteCourse(params: MfuRt02SaveCourse): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('/mfu/mfu-rt02/deleteCourse'), params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }

  //selectCourse
  public searchData(courseId: number): Observable<any> {
    let params = '?courseId=' + courseId;
    return this.authHttp.get(this.restServer.getAPI('/mfu/mfu-rt02/selectCourse') + params)
      .map(val => {
        return {
          records: val.json()
        }
      });
  }
}
