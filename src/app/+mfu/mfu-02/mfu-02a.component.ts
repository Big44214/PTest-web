import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MfuRt02Service } from './mfu-02.service';
import { MfuRt02Criteria, MfuRt02SaveCourse } from './mfu-02.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { BeanUtils } from '../../core/bean-utils.service';
import { ModalDirective } from 'ngx-bootstrap';
import { PageContext } from '../../shared/services/pageContext.service';
import { Page } from '../../shared/pagination.component';

@Component({
  selector: 'mfu-02a',
  templateUrl: ('./mfu-02a.component.html'),
  providers: [Page, MfuRt02Service]
})
export class MfuRt02aComponent implements AfterViewInit {

  @ViewChild('myForm') myform: any;
  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('backModal') public backModal: ModalDirective;

  private form: FormGroup = this.fb.group({
    courseId: this.fb.control(null)
    , courseCode: this.fb.control(null)
    , courseName: this.fb.control("")
  });

  private courseId: number = null;
  private courseCode: string = "";
  private courseName: string = "";


  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private objCriteria: MfuRt02SaveCourse = new MfuRt02SaveCourse();
  private objDelete: MfuRt02SaveCourse = new MfuRt02SaveCourse();
  private alert = { title: '', msg: '', isRedirect: false };
  private records: any[];

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: MfuRt02Service
    , private router: Router
    , private routerActive: ActivatedRoute
    , private pageContext: PageContext
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    if (BeanUtils.isNotEmpty(this.fromLoadObject.searchData)) {
      this.setData(this.fromLoadObject.searchData);
    }
  }


  ngAfterViewInit() {
    if (BeanUtils.isEmpty(this.courseId)) {
      // this.form.controls["keepStatusStatus"].setValue(1);
    }
  }

  //Save function

  public onSave() {
    if (this.form.dirty) {
      if (this.form.valid == true) {
        if (BeanUtils.isNotEmpty(this.courseId)) {
          this.objCriteria.courseId = this.courseId;
        }
        this.objCriteria.courseCode = this.form.controls["courseCode"].value;
        this.objCriteria.courseName = this.form.controls["courseName"].value;
        this.showMsg = false;
        this.service.saveCourse(this.objCriteria).subscribe(response => {
          this.records = response.records.data.courseId;
          this.courseId = response.records.data.courseId;
          if (response.records.data.checkCourseCode == 0) {
            this.alertModalToggle('สำเร็จ', 'บันทึกข้อมูลสำเร็จ', true);
            this.form.markAsPristine()
          } else if (response.records.data.checkCourseCode == 1) {
            this.alertModalToggle('แจ้งเตือน', 'รหัสรายวิชาซ้ำ', false);
          } else {
            this.alertModalToggle('ผิดพลาด', 'ไม่สามารถบันทึกข้อมูลได้', false);
          }
        },
          error => {
            console.error(error);
            this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
          }
        );
      } else {
        this.alertModalToggle('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล', false);
      }
    } else {
      this.alertModalToggle('แจ้งเตือน', 'ข้อมูลไม่ได้รับการแก้ไข', false);
    }
  }
  private setData(Object: any) {
    if (BeanUtils.isNotEmpty(Object)) {
      this.form.controls["courseCode"].setValue(Object.records.data.courseCode);
      this.form.controls["courseName"].setValue(Object.records.data.courseName);
      this.courseId = Object.records.data.courseId
    }
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.show();
  }

  //Back function

  private backModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.backModal.show();
  }

  private onBack(): void {
    if (this.form.dirty == false) {
      this.backPage();
    } else {
      this.backModalToggle('แจ้งเตือน', 'ข้อมูลได้รับการแก้ไข ต้องการเปลี่ยนหน้าหรือไม่ ?', false);
    }
  }

  private backPage(): void {
    this.pageContext.backPage();
  }

  private back() {
    this.alertModal.hide();
    if (this.alert.isRedirect) {
      this.backPage();
    }
  }

  public onCancel() {
    this.closeEvent.emit();
  }

  private alertModalClose(): void {
    if (null != this.records) {
      if (this.router.url.split("/")[2] == "mfu-02a") {
        this.pageContext.backPage();
      }
      if (this.router.url.split("/")[2] == "mfu-02A") {
        this.pageContext.backPage();
      }
    }
    this.alertModal.toggle();
  }

  //Delete function

  private deleteModalClose(): void {
    this.deleteModal.hide();
  }

  private deleteModalConfirm(): void {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่', true);
  }

  public onDelete() {
    this.objDelete.courseId = this.courseId
    this.service.deleteCourse(this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      this.pageContext.backPage();
    },
      error => {
        console.error(error);
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }


}


