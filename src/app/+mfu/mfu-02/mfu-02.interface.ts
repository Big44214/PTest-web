export class MfuRt02Criteria {
    public inputSearch: string = '';
    public courseId: number;
    public courseCode: string;
    public courseName: string;
}

export class MfuRt02SaveCourse {
    public courseId: number;
    public courseCode: string;
    public courseName: string;
}