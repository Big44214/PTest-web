import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Mfu02RoutingModule } from './mfu-02-routing.module';

import { MfuRt02Component } from './mfu-02.component';
import { MfuRt02aComponent } from './mfu-02a.component';
import { ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule } from 'ngx-bootstrap';


export const _IMPORTS = [
    SharedModule, Mfu02RoutingModule, ModalModule, TabsModule, TypeaheadModule, AlertModule, TooltipModule
]

export const _DECLARATIONS = [
    MfuRt02Component, MfuRt02aComponent
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class Mfu02Module { }
