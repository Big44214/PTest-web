
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MfuRt02Service } from './mfu-02.service';
import { MfuRt02Criteria, MfuRt02SaveCourse } from './mfu-02.interface';
import { ModalDirective } from 'ngx-bootstrap';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { PageContext } from '../../shared/services/pageContext.service';
import { SearchStatus } from '../../shared/constant/common.interface';
import { Page } from '../../shared/pagination.component';

@Component({
  selector: 'mfu-02',
  templateUrl: ('./mfu-02.component.html'),
  providers: [Page, MfuRt02Service]
})
export class MfuRt02Component {

  @Input('info') private info: any;
  @ViewChild('alertModal') public alertModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  private form: FormGroup = this.fb.group({
    inputSearch: this.fb.control("")
    , courseId: this.fb.control(null)
    , courseCode: this.fb.control(null)
    , courseName: this.fb.control("")
  });

  // private courseId: number = null;
  private courseCode: string = "";
  private courseName: string = "";


  private initMsg: string;
  private showMsg = false;
  private records: any[];
  private fromLoadObject: any;
  private flagForPaging: number = 0;
  private objCriteria: MfuRt02Criteria = new MfuRt02Criteria();
  private objDelete: MfuRt02SaveCourse = new MfuRt02SaveCourse();
  private alert = { title: '', msg: '', isRedirect: false };
  private inputSearch: string;
  private courseId: any;

  constructor(private page: Page
    , private fb: FormBuilder
    , private service: MfuRt02Service
    , private router: Router
    , private pageContext: PageContext
    , private routerActive: ActivatedRoute
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  public childModalShow(record: any) {
    this.pageContext.nextPage(['/mfu/mfu-02/mfu-02a', record.courseId], this.objCriteria);
  }

  public createCourse() {
    let courseId: any = "";
    this.pageContext.nextPage(['/mfu/mfu-02/mfu-02a', courseId], this.objCriteria);
  }

  // Search function

  ngOnInit() {
    if (this.pageContext.isLoad()) {
      let store = this.pageContext.loadPageState();
      this.objCriteria = store.pageContext;
      if (store.search.hasSearch) {
        this[store.search.method](...store.search.args);
      }
    }
  }

  private onSearch(modeSearch: number) {
    if (BeanUtils.isNotEmpty(modeSearch)) {
      this.flagForPaging = modeSearch
      this.page.start = 0;
      if (modeSearch == SearchStatus.normalSearch) {
        this.onNormalSearch(this.page);
      }
    }
  }

  public onNormalSearch(changeEvent: Page) {
    this.pageContext.setLastSearch('onNormalSearch', [this.page]);
    console.log(this.form.controls["inputSearch"].value)
    this.objCriteria.inputSearch = this.form.controls["inputSearch"].value;
    this.showMsg = false;
    if (BeanUtils.isNotEmpty(changeEvent)) {
      this.page = changeEvent;
    }
    this.callServiceSearch(this.page, this.objCriteria);
  }

  private callServiceSearch(page: Page, mapping: MfuRt02Criteria) {
    this.service.searchCourse(page, mapping).subscribe(response => {
      this.page = response.page;
      this.records = response.records;
      if (BeanUtils.isNotEmpty(this.records)) {

      } else {
        this.alertModalToggle('แจ้งเตือน', 'ไม่พบข้อมูล', false);
      }
    },
      error => {
        console.error(error);
      }
    );
  }

  private alertModalToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.alertModal.toggle();
  }

  public closeAlert() {
    this.initMsg = "";
    this.showMsg = false;
  }

  //Delete function

  private deleteModalClose(): void {
    this.deleteModal.hide();
  }

  private deleteModalConfirm(): void {
    this.onDelete();
    this.deleteModal.hide();
  }

  private alertModalDeleteToggle(title: string, msg: string, isRedirect: boolean): void {
    this.alert.title = title;
    this.alert.msg = msg;
    this.alert.isRedirect = isRedirect;
    this.deleteModal.toggle();
  }

  public onBeforeDelete(record: any) {
    this.alertModalDeleteToggle('ยืนยันการลบ', 'คุณยืนยันการลบหรือไม่', true);
    this.courseId = record.courseId
  }

  public onDelete() {
    this.objDelete.courseId = this.courseId
    this.service.deleteCourse(this.objDelete).subscribe(response => {
      this.alertModalToggle('สำเร็จ', 'ลบข้อมูลสำเร็จ', true);
      this.onSearch(0);
    },
      error => {
        console.error(error);
        this.alertModalToggle('ผิดพลาด', 'กรุณาติดต่อผู้ดูแลระบบ', false);
      }
    );
  }

}






























