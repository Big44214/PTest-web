import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BeanUtils } from '../../core/bean-utils.service';
import { MfuRt02Service } from './mfu-02.service';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class MfuRt02Resolve implements Resolve<any> {

    constructor(private MfuRt02Service: MfuRt02Service) { }

    resolve(route: ActivatedRouteSnapshot): any {

        if (route.url[0].path == 'mfu-02') {
            
            return {

            };
        } else if (route.url[0].path == 'mfu-02a') {
    
            if (BeanUtils.isNotEmpty(route.params['courseId'])) {
                var searchData = this.MfuRt02Service.searchData(route.params['courseId']);
                return Observable.forkJoin([searchData]).map((response) => {
                    return {
                        searchData: response[0]
                    };
                })
            } else {

                return {

                };
            }
        } else if (route.url[0].path == 'mfu-02A') {


            if (BeanUtils.isNotEmpty(route.params['courseId'])) {
                var searchData = this.MfuRt02Service.searchData(route.params['courseId']);
                return Observable.forkJoin([searchData]).map((response) => {
                   
                    return {
                        searchData: response[0]
                    };
                })
            } else {

                return {

                };
            }
        }

    }
}
