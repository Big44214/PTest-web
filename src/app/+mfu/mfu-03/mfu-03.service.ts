import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { RestServerService } from '../../core/services/rest-server.service';
import { Department, SaveDepartment } from './mfu-03.interface';;


@Injectable()
export class Mfu03Service {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  //Search Department
  public searchDepartment(): Observable<any> {
    let params;
    return this.authHttp.post(this.restServer.getAPI('mfu/mfu-03/searchDepartment'), params)
      .map(response => {
        return response.json();
      });;
  }

  //Select Department
  public selectDepartment(departmentId: number): Observable<any> {
    let params = '?&departmentId=' + departmentId;
    return this.authHttp.get(this.restServer.getAPI('mfu/mfu-03/selectDepartment') + params)
      .map(response => {
        return response.json();
      });;
  }

  //Save Department
  public saveDepartment(params: SaveDepartment): Observable<any> {
    return this.authHttp.post(this.restServer.getAPI('mfu/mfu-03/saveDepartment'), params)
      .map(response => {
        return response.json();
      });;
  }
}
