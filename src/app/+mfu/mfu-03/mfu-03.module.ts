import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Mfu03Component } from './mfu-03.component';
import { Mfu03aComponent } from './mfu-03a.component';
import { Mfu03RoutingModule } from './mfu-03-routing.module';

export const _IMPORTS = [
    SharedModule, Mfu03RoutingModule
]

export const _DECLARATIONS = [
    Mfu03Component, Mfu03aComponent
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS   
})
export class Mfu03Module { }
