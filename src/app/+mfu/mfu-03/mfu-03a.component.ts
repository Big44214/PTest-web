
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mfu03Service } from './mfu-03.service';
import { Department, StudentInDepartmentTable, SaveDepartment } from './mfu-03.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { BeanUtils } from '../../core/bean-utils.service';
import { SelectItem } from "primeng/primeng";


@Component({
  selector: 'app-mfu-03a',
  templateUrl: ('./mfu-03a.component.html'),
  styleUrls: ['./mfu-03.component.scss'],
  providers: [Mfu03Service]
})
export class Mfu03aComponent {

  @Output('closeModal') private closeEvent = new EventEmitter();

  private form: FormGroup;
  private studentNoDepartment: SelectItem[];
  private studentInDepartmentTable: StudentInDepartmentTable[];
  private newStudentInDepartmentTable: StudentInDepartmentTable;

  private saveDepartmentObj: SaveDepartment = new SaveDepartment();
  private fromLoadObject: any;
  private departmentId: number;
  private initMsg: string;
  private showMsg = false;
  private alert = { title: '', msg: '' };

  constructor(private fb: FormBuilder
    , private service: Mfu03Service
    , private router: Router
    , private routerActive: ActivatedRoute
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
    this.form = fb.group({
      departmentCode: fb.control("", Validators.required),
      departmentName: fb.control("", Validators.required)
    });
  }

  ngOnInit() {

    this.studentNoDepartment = []
    this.studentNoDepartment.push({ label: 'Select Student', value: null });
    this.fromLoadObject.studentNoDepartment.data.forEach(val => {
      this.studentNoDepartment.push({ label: val.studentCode, value: val.studentCode });
    });

    let selectDepartment = this.fromLoadObject.selectDepartment;
    this.studentInDepartmentTable = selectDepartment.studentList;
    this.form.controls['departmentCode'].setValue(selectDepartment.department.departmentCode);
    this.form.controls['departmentName'].setValue(selectDepartment.department.departmentName);
    this.departmentId = selectDepartment.department.departmentId;
  }

  studentChange(event, index) {
    let row = this.studentInDepartmentTable.find((val, i) => i == index);
    if (event.value != null) {
      row.studentName = this.fromLoadObject.studentNoDepartment.data.find(
        val => val.studentCode == event.value
      ).studentName;
    } else {
      row.studentCode = 'Select Student';
      row.studentName = '';
    }
  }

  addRow() {
    if (this.form.valid) {
      this.newStudentInDepartmentTable = new NewStudentInDepartmentTable();
      this.newStudentInDepartmentTable.studentCode = 'Select Student';
      let studentInDepartmentTable = [...this.studentInDepartmentTable];
      studentInDepartmentTable.push(this.newStudentInDepartmentTable);
      this.studentInDepartmentTable = studentInDepartmentTable;
    }
  }

  deleteRow(index) {
    this.studentInDepartmentTable = this.studentInDepartmentTable.filter((val, i) => i != index);
  }

  onSave() {
    if (this.form.valid == true) {
      if (BeanUtils.isNotEmpty(this.departmentId)) {
        this.saveDepartmentObj.departmentId = this.departmentId;
      }
      this.saveDepartmentObj.departmentCode = this.form.controls["departmentCode"].value;
      this.saveDepartmentObj.departmentName = this.form.controls["departmentName"].value;
      this.saveDepartmentObj.studentList = this.studentInDepartmentTable;

      this.showMsg = false;
      this.service.saveDepartment(this.saveDepartmentObj).subscribe(response => {
        this.departmentId = response.data.departmentId;
      }, );
    }
  }

}

class NewStudentInDepartmentTable implements StudentInDepartmentTable {
  constructor(
    public departmentId?,
    public studentId?,
    public studentName?,
    public studentCode?,
  ) { }
}