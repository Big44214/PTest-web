
import { Component, OnChanges, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mfu03Service } from './mfu-03.service';
import { SelectItem } from 'primeng/primeng';
import { Department, DepartmentTable} from './mfu-03.interface';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { CommonComboboxService } from "../../shared/services/comboboxs/common/common-combobox.service";

@Component({
  selector: 'mfu-03',
  templateUrl: ('./mfu-03.component.html'),
  styleUrls: ['./mfu-03.component.scss'],
  providers: [Mfu03Service]
})
export class Mfu03Component {

  @Output('closeModal') private closeEvent = new EventEmitter();
  @Input('info') private info: any;

  private departmentObj: Department;
  private departmentTable: DepartmentTable[];

  private fromLoadObject: any;
  private initMsg: string;
  private showMsg = false;
  private alert = { title: '', msg: '' };
  private records: any[];

  constructor(private fb: FormBuilder
    , private router: Router
    , private routerActive: ActivatedRoute
  ) {
    this.fromLoadObject = this.routerActive.snapshot.data['fromLoadObject'];
  }

  ngOnInit() {

    this.departmentTable = this.fromLoadObject.searchDepartment.data;

  }

  public editDepartment(departmentId: any) {
    this.router.navigate(['/mfu/mfu-03/01a/1', departmentId], { skipLocationChange: true });
  }

  public createDepartment() {
    var departmentId = new Department();
    this.router.navigate(['/mfu/mfu-03/01a/1', departmentId], { skipLocationChange: true });
  }

  public onRowClick(event){
    this.editDepartment(event.data.departmentId);
  }

}
class NewDepartmentTable implements DepartmentTable {
  constructor(
    public departmentId?,
    public departmentName?,
    public departmentCode?,
    public numberOfStudent?
  ) { }
}