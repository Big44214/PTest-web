import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/guard/auth.guard';

// Component.
import { Mfu03Component } from './mfu-03.component';
import { Mfu03aComponent } from './mfu-03a.component';

// Resolve.
import { Mfu03Resolve } from './mfu-03.resolve';

// Service.
import { Mfu03Service } from './mfu-03.service';

const routes: Routes = [{
    path: '',
    children: [{
        path: '01/:refresh'
        , component: Mfu03Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: Mfu03Resolve }
    },  {
        path: '01a/:refresh/:departmentId'
        , component: Mfu03aComponent
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: Mfu03Resolve }
    }, {
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [Mfu03Service,Mfu03Resolve]
})
export class Mfu03RoutingModule { }
