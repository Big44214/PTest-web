import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Mfu03Service } from './mfu-03.service';
import { CommonComboboxService } from "../../shared/services/comboboxs/common/common-combobox.service";

import 'rxjs/add/observable/forkJoin';

@Injectable()
export class Mfu03Resolve implements Resolve<any> {

    constructor(
        private mfu03Service: Mfu03Service,
        private commonComboboxService: CommonComboboxService
    ) { }

    resolve(route: ActivatedRouteSnapshot): any {
        if (route.url[0].path == '01') {
            var searchDepartment = this.mfu03Service.searchDepartment();
            return Observable.forkJoin([searchDepartment]).map((response) => {
                return {
                    searchDepartment: response[0]
                };
            });
        } else if(route.url[0].path == '01a'){
            var studentNoDepartment = this.commonComboboxService.getStudentNoDepartment();
            var selectDepartment = this.mfu03Service.selectDepartment(route.params['departmentId']);
            return Observable.forkJoin([studentNoDepartment, selectDepartment]).map((response) => {
                return {
                    studentNoDepartment: response[0],
                    selectDepartment: response[1]
                };
            });
        }
    }
}