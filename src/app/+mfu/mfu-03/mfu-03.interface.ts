export class Department {
    public departmentId: number;
    public departmentName: string;
    public departmentCode: string;
}

export class SaveDepartment {
    public departmentId: number;
    public departmentName: string;
    public departmentCode: string;
    public studentList: StudentInDepartmentTable[];
}

export interface StudentInDepartmentTable {
    departmentId?;
    studentId?;
    studentName?;
    studentCode?;
}

export interface DepartmentTable {
    departmentId?;
    departmentName?;
    departmentCode?;
    numberOfStudent?;
}