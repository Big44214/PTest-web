import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MfuComponent } from './mfu.component';
import { MfuRoutingModule } from './mfu-routing.module';
import { MfuDashboardComponent } from "./mfu-dashboard/mfu-dashboard.component";
import { PageContext } from './../shared/services/pageContext.service';
@NgModule({
    imports: [
        SharedModule, MfuRoutingModule
    ]
    , declarations: [
        MfuComponent, MfuDashboardComponent
    ]
    , providers: [

        PageContext
    ]
})
export class MfuModule { }
