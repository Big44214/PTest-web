import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrainComponent } from './train.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { TrainDashboardComponent } from "app/+train/train-dashboard/train-dashboard.component";
import { TrainDt01Component } from "app/+train/train-dt/train-dt-01/train-dt-01.component";
import { TrainDt02Component } from "app/+train/train-dt/train-dt-02/train-dt-02.component";
import { TrainDt03Component } from "app/+train/train-dt/train-dt-03/train-dt-03.component";

const routes: Routes = [{
    path: '', component: TrainComponent
    , children: [
        { 
            path: 'dashboard'
            , component: TrainDashboardComponent
            , canActivateChild: [AuthGuard]
        }, {
            path: 'dt-01'
            , component: TrainDt01Component
            , canActivateChild: [AuthGuard]
        }, {
            path: 'dt-02'
            , component: TrainDt02Component
            , canActivateChild: [AuthGuard]
        },{
            path: 'dt-03'
            , component: TrainDt03Component
            , canActivateChild: [AuthGuard]
        },{ 
            path: ''
            , redirectTo: 'dashboard'
            , pathMatch: 'full' 
        }
    ]
   }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ]
    , exports: [ RouterModule ]
})
export class TrainRoutingModule { }

