import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
// import { TrainDashboardRoutingModule } from './mfu-dashboard-routing.module';
import { TrainDt03Component } from './train-dt-03.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule, Ng2Charts
    ],
    declarations: [
        TrainDt03Component 
    ]
})
export class TrainDt03Module { }