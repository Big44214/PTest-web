import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
// import { TrainDashboardRoutingModule } from './mfu-dashboard-routing.module';
import { TrainDt01Component } from './train-dt-01.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule, Ng2Charts
    ],
    declarations: [
        TrainDt01Component 
    ]
})
export class TrainDt01Module { }