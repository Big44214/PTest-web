import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
// import { TrainDashboardRoutingModule } from './mfu-dashboard-routing.module';
import { TrainDt02Component } from './train-dt-02.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule, Ng2Charts
    ],
    declarations: [
        TrainDt02Component 
    ]
})
export class TrainDt01Module { }