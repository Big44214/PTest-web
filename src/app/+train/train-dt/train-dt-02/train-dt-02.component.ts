import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TrainDt01Service } from "app/+train/train.service";

@Component({
    selector: 'dt-02'
    , templateUrl: './train-dt-02.component.html'
    , styleUrls: ['./train-dt-02.scss']
    , providers: [TrainDt01Service]
})
export class TrainDt02Component implements OnInit {
    form: FormGroup;
    
    constructor(@Inject(FormBuilder) fb: FormBuilder
        , private router: Router
        , private service: TrainDt01Service) {
            
            this.form = fb.group({
                typeItem: fb.control("", Validators.required)
            });
        }
    
    eventChangeTypeItem() {
        console.log(this.form.controls["typeItem"].value);
    }

    ngOnInit() { }
}
