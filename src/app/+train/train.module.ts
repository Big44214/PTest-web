import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TrainComponent } from './train.component';
import { TrainRoutingModule } from './train-routing.module';
import { TrainDt01Component } from './train-dt/train-dt-01/train-dt-01.component'
import { TrainDashboardComponent } from "app/+train/train-dashboard/train-dashboard.component";
import { TrainDt02Component } from "app/+train/train-dt/train-dt-02/train-dt-02.component";
import { TrainDt03Component } from "app/+train/train-dt/train-dt-03/train-dt-03.component";

@NgModule({
    imports: [
        SharedModule, TrainRoutingModule
    ]
    , declarations: [
        TrainComponent, TrainDt01Component, TrainDt02Component , TrainDt03Component ,TrainDashboardComponent
    ]
})
export class TrainModule { }

