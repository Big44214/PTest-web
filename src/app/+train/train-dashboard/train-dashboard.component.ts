import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-train-dashboard'
    , templateUrl: './train-dashboard.component.html'
    , styleUrls: ['./train-dashboard.scss']
})
export class TrainDashboardComponent implements OnInit {
    constructor() { }
    ngOnInit() { }
}
