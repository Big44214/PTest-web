import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { TrainDt01Service } from './train.service';


@Component({
    selector: 'app-train'
    , templateUrl: './train.component.html'
    , styleUrls: ['./train.component.scss']
    , providers: [TrainDt01Service]
})


export class TrainComponent implements OnInit {

    form: FormGroup;
    
    constructor(@Inject(FormBuilder) fb: FormBuilder
        , private router: Router
        , private service: TrainDt01Service) {
            
            this.form = fb.group({
                typeItem: fb.control("", Validators.required)
            });

        }

    ngOnInit() {
        // if (this.router.url === '/') {
        //     this.router.navigate(['/dashboard']);
        // }
        // this.form('typeItem')
    }
}
