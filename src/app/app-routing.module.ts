import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { RoleGuard } from './shared/guard/role.guard';
import { CanDeactivateGuard } from './shared/services/can-deactivate-guard.service';
import { AppCustomPreloader } from './app-routing-loader';

const routes: Routes = [{ 
        path: 'inv'
        , loadChildren: './+in/in.module#InModule'
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin'], preload: false }
    }, { 
        path: 'pos'
        , loadChildren: './+pos/pos.module#PosModule'
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin', 'user'], preload: true }
    }, { 
        path: 'mfu'
        , loadChildren: './+mfu/mfu.module#MfuModule'
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin', 'user'], preload: true }
    }, { 
        path: 'train'
        , loadChildren: './+train/train.module#TrainModule'
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin', 'user'], preload: true }
    },{ 
        path: ''
        , loadChildren: './+su/su.module#SuModule'
        // , data: { preload: true }
    }
];

@NgModule({
      imports: [ RouterModule.forRoot(routes, { preloadingStrategy: AppCustomPreloader }) ]
    , exports: [ RouterModule ]
    , providers: [ CanDeactivateGuard, AppCustomPreloader ]
})
export class AppRoutingModule { }
