import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

//For Translate language.
import { TranslateModule } from '@ngx-translate/core';

//For boostrap4.
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { ComponentsModule } from './components/components.module';
import { DirectiveModule } from './directive/directive.module';
import { DatepickerModule } from 'ngx-bootstrap';
import { SharedPipesModule } from './pipes/shared-pipes.module';
import { NgPipesModule } from 'ngx-pipes';
import { CurrencyMaskModule } from "ng2-currency-mask";

import { InputTextModule, ButtonModule, DataTableModule, DialogModule, AutoCompleteModule
        , CalendarModule, InputTextareaModule, MultiSelectModule, DropdownModule, SelectButtonModule
        , OverlayPanelModule, GalleriaModule, PanelModule, InputMaskModule, PasswordModule} from 'primeng/primeng';

export const _IMPORTS = [
    CommonModule, FormsModule, RouterModule, NgbModule, ReactiveFormsModule
    , BsDropdownModule.forRoot(), DatepickerModule.forRoot(), TabsModule.forRoot()
    , InputTextModule, ButtonModule, DataTableModule, DialogModule, AutoCompleteModule
    , CalendarModule, InputTextareaModule, MultiSelectModule, DropdownModule, SelectButtonModule
    , OverlayPanelModule, NgPipesModule, GalleriaModule, PanelModule, InputMaskModule
    , PasswordModule, CurrencyMaskModule, SharedPipesModule
]

export const _EXPORTS = [
    CommonModule, FormsModule, TranslateModule, NgbModule
    , ComponentsModule, ReactiveFormsModule, BsDropdownModule, TabsModule, DirectiveModule
    , InputTextModule, ButtonModule, DataTableModule, DialogModule, AutoCompleteModule
    , CalendarModule, InputTextareaModule, MultiSelectModule, DropdownModule, SelectButtonModule
    , OverlayPanelModule, NgPipesModule, GalleriaModule, PanelModule, InputMaskModule
    , PasswordModule, CurrencyMaskModule, SharedPipesModule
]

@NgModule({
    imports: _IMPORTS
  , exports: _EXPORTS
})
export class SharedModule { }