import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-sticky-footer'
    , templateUrl: './sticky-footer.component.html'
    , styleUrls: ['./sticky-footer.component.scss']
})
export class StickyFooterComponent {
    @Input() heading: string;
    @Input() icon: string;
}
