import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-card'
    , templateUrl: './card.component.html'
    , styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    @Input() bgClass: string;
    @Input() icon: string;
    @Input() count: number;
    @Input() label: string;
    @Input() routingToURL: string = "/not-found";
    @Input() data: string = "View Details.";
    @Output() event: EventEmitter<any> = new EventEmitter();

    constructor(private router: Router) { }
    ngOnInit() { }
    
    private routingTo(){
        this.router.navigate([this.routingToURL]);
    }
}
