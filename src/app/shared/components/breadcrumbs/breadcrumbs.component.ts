import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-breadcrumbs'
  , templateUrl: './breadcrumbs.component.html'
  , styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input() url: string = "";
  breadcrumbs = [];
  private oldUrl: string = "";

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    let arrayMax = this.url.split("/").length;
    this.url.split("/").forEach((data, index) => {
      this.oldUrl += "/"+data;
      if(arrayMax-1 == index){
        this.breadcrumbs.push({
          label: data
          , url: this.oldUrl
          , last: true
        })
      }else{
        this.breadcrumbs.push({
          label: data
          , url: this.oldUrl
          , last: false
        })
      }
    });
  }
}
