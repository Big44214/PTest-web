import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

//For boostrap4.
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';

//For Translate language.
import { TranslateModule } from '@ngx-translate/core';

//For boostrap4.
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DirectiveModule } from '../directive/directive.module';
import { BlockUIModule } from 'ng-block-ui';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarToolComponent } from './sidebar-tool/sidebar-tool.component';
import { CardComponent } from './card/card.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { StickyFooterComponent } from './sticky-footer/sticky-footer.component';
import { BlockTemplateComponent } from './block-template/block-template.component';
import { PanelModule } from 'primeng/primeng';
import { NgxGalleryModule } from 'ngx-gallery';

export const _IMPORTS = [
    CommonModule, FormsModule, HttpModule 
    , TranslateModule, NgbModule, RouterModule
    , BsDropdownModule.forRoot(), TabsModule.forRoot(), DirectiveModule
    , BlockUIModule, PanelModule, NgxGalleryModule
]

export const _EXPORTS = [
    HeaderComponent, SidebarComponent, CardComponent
    , PageHeaderComponent, BreadcrumbsComponent, StickyFooterComponent
    , SidebarToolComponent, BlockTemplateComponent, BlockUIModule, PanelModule
    , NgxGalleryModule
]

export const _DECLARATIONS = [
    HeaderComponent, SidebarComponent, CardComponent
    , PageHeaderComponent, BreadcrumbsComponent, StickyFooterComponent
    , SidebarToolComponent, BlockTemplateComponent
]

@NgModule({
    imports: _IMPORTS
    , exports: _EXPORTS
    , declarations: _DECLARATIONS
    , entryComponents: [
        BlockTemplateComponent
    ]
})
export class ComponentsModule { }
