import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar'
    , templateUrl: './sidebar.component.html'
    , styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit  {
    @Input() changeModule: string = null; 
    listMenu = [];

    constructor() {}

    ngOnInit() {
        if(this.changeModule == 'in'){
            this.listMenu.push({
            url: '/inv/dashboard', icon: 'fa fa-fw fa-dashboard', label: 'dashboard' }
            , { url: '/inv/daily/receiving/1', icon: 'fa fa-fw fa-download', label: 'รายการรับสินค้า' }
            , { url: '/inv/daily/saling/1', icon: 'fa fa-fw fa-upload', label: 'รายการจ่ายสินค้า' }
            , { url: '/inv/daily/dashboard', icon: 'fa fa-fw fa-calendar', label: 'รายการประจำวัน' }
            , { url: '/inv/master/dashboard', icon: 'fa fa-fw fa-folder-open-o', label: 'แฟ้มข้อมูลหลัก' }
            );
        }else if(this.changeModule == 'pos'){
            this.listMenu.push({
            url: '/pos/dashboard', icon: 'fa fa-fw fa-dashboard', label: 'dashboard'}
            , { url: '/pos/daily/purchaseDetail', icon: 'fa fa-fw fa-exchange', label: 'ขาย - ซื้อ'}
            );
        }else if(this.changeModule == 'su'){
            this.listMenu.push({
            url: '/su/user', icon: 'fa fa-fw fa-user-circle', label: 'ข้อมูลทั่วไป'}
            , { url: '/su/password', icon: 'fa fa-fw fa-unlock-alt', label: 'รหัสผ่าน'}
            );
        }else if(this.changeModule == 'mfu'){
            this.listMenu.push(
                {url: '/mfu/dashboard', icon: 'fa fa-fw fa-folder-open-o', label: 'Home'}
            );
        }else if(this.changeModule == 'train'){
            this.listMenu.push(
                {url: '/train/dashboard', icon: 'fa fa-fw fa-folder-open-o', label: 'Home'}
                , {url: '/train/dt-01', icon: 'fa fa-fw fa-dashboard', label: 'ข้อมูลหลักสูตร'}
                , {url: '/train/dt-02', icon: 'fa fa-fw fa-user-circle', label: 'สร้างแบบการประเมิน'}
            );
        }
    }
}
