import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-sidebar-tool'
    , templateUrl: './sidebar-tool.component.html'
    , styleUrls: ['./sidebar-tool.component.scss']
})
export class SidebarToolComponent implements OnInit  {
    @Input() menuStatus: string = null; 
    listMenu = [];

    constructor() {}

    private menuFucntion(func){
    console.info(func);
    }

    ngOnInit() {
        if(this.menuStatus == '12'){
            this.listMenu.push({
            function: 'save()', icon: 'fa fa-fw fa-save fa-2x', label: 'Save' }
            , { function: 'search()', icon: 'fa fa-fw fa-search fa-2x', label: 'Search' }
            );
        }
    }

    private save(){
        console.info("save");
    }
}
