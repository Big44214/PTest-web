import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../../core/services/user.service';

@Component({
    selector: 'app-header'
    , templateUrl: './header.component.html'
    , styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  username: string = "";
  
  constructor(private translate: TranslateService, private authService: AuthService
        , public router: Router, private userService: UserService) {
         this.username = userService.getUserName();
  }

  ngOnInit(): void {}

  changeLang(language: string) {
    this.translate.use(language);
  }

  onLoggedout() {
      this.authService.logout();
  }

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

}
