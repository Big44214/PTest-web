import { Component } from '@angular/core';

@Component({
  selector: 'block-temp',
  styles: [`
    :host {
      text-align: center;
      color: #BE132D;
    }
  `],
  template: `
    <div class="block-ui-template">
      <i class="fa fa-cog fa-spin fa-5x" style="color:#my-red" aria-hidden="true"></i>
      <div><h1 style="color:#FFF"><strong>{{message}}</strong></h1></div>
    </div>
  `
})
export class BlockTemplateComponent {
  constructor() {}
}