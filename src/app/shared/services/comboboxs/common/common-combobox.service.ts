import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { SelectItem } from 'primeng/primeng';
import { RestServerService } from "../../../../core/services/rest-server.service";
import { Observable } from "rxjs/Observable";


@Injectable()
export class CommonComboboxService {

    private documentStatus: SelectItem[];
    private department: SelectItem[];

    constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

    private getRemoteSelect(url: string) {
        return this.authHttp.get(this.restServer.getAPI(url)).map((response) => { return response.json(); });
    }

    private isObservableEmpty(value: any): boolean {
        return !(value.source && value.operator);
    }

    //Inv status document.
    public getStatusDocument(): SelectItem[]{
        this.documentStatus = [];
        this.documentStatus.push({label:'ยกเลิก', value:0});
        this.documentStatus.push({label:'ปกติ', value:1});
        return this.documentStatus;
    }

    //Department.
    private _department: Observable<any> = new Observable<any>();
    public getDepartment(): Observable<any>{
        let url = 'common/combobox/department';
        this._department = this.getRemoteSelect(url);
        return this._department;
    }

    //Student With Out Department.
    private _studentNoDepartment: Observable<any> = new Observable<any>();
    public getStudentNoDepartment(): Observable<any>{
        let url = 'common/combobox/studentNoDepartment';
        this._studentNoDepartment = this.getRemoteSelect(url);
        return this._studentNoDepartment;
    }

};
