import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { RestServerService } from '../../core/services/rest-server.service';

@Injectable()
export class HttpService {

  constructor(public authHttp: AuthHttp, private restServer: RestServerService) { }

  public save(funtionDB: String, args?: Object){
    var json = this.buildParams(funtionDB, args);
    return this.authHttp.post(this.restServer.getAPI('jsonString'), JSON.stringify(json));
  }

  public search(funtionDB: String, args?: Object){
    var json = this.buildParams(funtionDB, args);
    return this.authHttp.post(this.restServer.getAPI('jsonString'), JSON.stringify(json));
  }

  public register(funtionDB: String, args?: Object){
    var json = this.buildParams(funtionDB, args);
    return this.authHttp.post(this.restServer.getAPI('register'), JSON.stringify(json));
  }

  private buildParams = (funtionDB: String, args?: Object): {method: String; data: object} => {   
    return{
        method: funtionDB,
        data: args
    };
  }
};
