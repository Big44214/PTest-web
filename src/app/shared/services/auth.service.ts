import { Injectable, Component } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { CookieService } from 'ngx-cookie';
import { JwtHelper } from 'angular2-jwt';
import { UserService } from '../../core/services/user.service';
import * as context from '../../core/interfaces/context-part.interface';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';

const http_basic: string = context.http_basic;
const base_authserver_url = context.base_authserver_url;
const csrf_token_uri: string = context.csrf_token_uri;
const login_uri: string = context.login_uri;
const logout_uri: string = context.logout_uri;
const ACCESS_TOKEN_KEY: string = context.ACCESS_TOKEN_KEY;
const CSRF_TOKEN_HEADER: string = context.CSRF_TOKEN_HEADER;
const CSRF_TOKEN: string = context.CSRF_TOKEN;

@Injectable()
export class AuthService {
  // store the URL so we can redirect after logging in
  private jwt: any;
  private csrfToken: any;
  private jwtHelper: JwtHelper = new JwtHelper();
  private _loginAnnounceSource = new Subject<any>();
  redirectUrl: string;
  username: string;
  isLoggedIn: boolean = false;
  isAuthened = this._loginAnnounceSource.asObservable();

  constructor(private http: Http, private cookieService:CookieService
    , private userService: UserService) {}

  getCsrfToken() {
    let body = '';
    let options = new RequestOptions({ withCredentials: true });
    this.http.get(csrf_token_uri, options)
      .map((res, err) => res.json())
      .subscribe((csrf) => {
        // console.log('csrf token: ' + csrf.token);
        if (csrf.delegate) {
          this.csrfToken = csrf.delegate;
        } else {
          this.csrfToken = csrf;
        }
        //XSRF-TOKEN Angular2 spec.
        this.cookieService.put('XSRF-TOKEN', this.csrfToken.token);
      },
      error => {
        console.log('error: ' + error);
      });
  }

  login(u: string, p: string): Observable<any> {
    if (this.isLoggedIn) {
      console.log('already authenticated');
      this._loginAnnounceSource.next(this.isLoggedIn);
      return
    }
    let headers = new Headers();
    headers.append('Authorization', `Basic ${http_basic}`);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append(this.csrfToken.headerName, this.csrfToken.token);
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = `username=${u}&password=${p}`;

    localStorage.setItem(CSRF_TOKEN_HEADER, this.csrfToken.headerName);
    localStorage.setItem(CSRF_TOKEN, this.csrfToken.token);

    return this.http.post(login_uri, body, options).map(response => {
      let result = response.json();
      console.log(result);
      if (result.access_token) {
        localStorage.setItem(ACCESS_TOKEN_KEY, result.access_token);
        // Sample: {
        //   "alg": "RS256",
        //     "typ": "JWT"
        // }
        // {
        //   "exp": 1474362608,
        //     "user_name": "siritas_s",
        //     "authorities": [
        //         "ROLE_TRUSTED_CLIENT"
        //       ],
        //     "jti": "e98fe210-b4f5-4d54-984f-1408da02ef90",
        //     "client_id": "erp-svr-client",
        //     "scope": [
        //     "read",
        //     "write"
        //             ]
        // }
        this.jwt = this.jwtHelper.decodeToken(result.access_token);
        this.isLoggedIn = true;
        this.userService.setRole(this.jwt.authorities[0]);
        this.userService.setUserName(this.jwt.user_name);
        return this.isLoggedIn;
        // return;
      } else {
        //{"error":"invalid_grant","error_description":"Bad credentials"}
        throw Observable.throw(result.error_description);
      }
    }).catch(error => {
      this.isLoggedIn = false;
      // let err = error.json();
      console.log(error)
      return Observable.throw(error);
    });
  }

  logout() {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    this.isLoggedIn = false;
  }

  checkAuthen() {
    let token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if (token) {
      this.jwt = this.jwtHelper.decodeToken(token);
      this.isLoggedIn = !this.jwtHelper.isTokenExpired(token);
      console.log('token expired? '+this.isLoggedIn);
      this.userService.setRole(this.jwt.authorities[0]);
      this.userService.setUserName(this.jwt.user_name);
    }
    this._loginAnnounceSource.next(this.isLoggedIn);
  }

}
