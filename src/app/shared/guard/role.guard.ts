import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot
        , NavigationExtras, CanActivateChild, CanLoad, Route } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { AuthService } from '../services/auth.service';

@Injectable()
export class RoleGuard implements CanActivate{

    constructor(private authService: AuthService, private userService: UserService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        // if(this.authService.isLoggedIn){
        //     if(this.userService.getRole() == 'user' && state.url == '/home'){
        //         this.router.navigate(['/pos'], {});
        //     }else{
        //         let roles = route.data["roles"] as Array<string>;
        //         let checkRole = roles.filter((role)=> {
        //             return  role === this.userService.getRole();
        //         });
        //         if (checkRole.length > 0) {
        //             return true;
        //         }else{
        //             this.router.navigate(['/home'], {});
        //         }
        //     }
        // }
        return true;
    }
}