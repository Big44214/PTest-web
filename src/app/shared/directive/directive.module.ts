import { NgModule } from '@angular/core';
import { AsideToggleDirective } from './aside.directive';
import { NavDropdownDirective, NavDropdownToggleDirective, NAV_DROPDOWN_DIRECTIVES } from './nav-dropdown.directive';
import { SidebarToggleDirective, SidebarMinimizeDirective, MobileSidebarToggleDirective
    , SidebarOffCanvasCloseDirective, SIDEBAR_TOGGLE_DIRECTIVES } from './sidebar.directive';

export const _EXPORTS = [
    AsideToggleDirective, NavDropdownDirective, NavDropdownToggleDirective
    , NAV_DROPDOWN_DIRECTIVES, SidebarToggleDirective, SidebarMinimizeDirective
    , MobileSidebarToggleDirective, SidebarOffCanvasCloseDirective, SIDEBAR_TOGGLE_DIRECTIVES
]

export const _DECLARATIONS = [
    AsideToggleDirective, NavDropdownDirective, NavDropdownToggleDirective
    , NAV_DROPDOWN_DIRECTIVES, SidebarToggleDirective, SidebarMinimizeDirective
    , MobileSidebarToggleDirective, SidebarOffCanvasCloseDirective, SIDEBAR_TOGGLE_DIRECTIVES
]

@NgModule({
    imports: [ ]
    , exports: _EXPORTS
    , declarations: _DECLARATIONS
})
export class DirectiveModule { }
