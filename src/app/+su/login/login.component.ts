import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-login'
    , templateUrl: './login.component.html'
    , styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
    message: string;
    username: string = 'admin';
    password: string = 'admin';
    csrfToken: string;
    subscription: Subscription;
    projectName = 'projectName';

    constructor(public authService: AuthService, public router: Router) {
        this.subscription = this.authService.isAuthened.subscribe(() => {
            console.log('username? ' + this.authService.username);
            console.log('is LoggedIn? ' + this.authService.isLoggedIn);
            if (this.authService.isLoggedIn) {
                this.redirectPage();
            }
        });
    }

    ngOnInit() {
        console.log('ngOnInit()');
        this.authService.checkAuthen();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    ngAfterViewInit() {
        console.log('ngAfterViewInit()');
        this.authService.getCsrfToken();
    }

    setMessage() {
        this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out')
    }

    onLoggedin() {
        this.message = 'Trying to log in ...';

        if (!this.username && !this.password) {
            this.message = 'blank credentials';
            return;
        }

        this.authService.login(this.username, this.password).subscribe((result) => {
            this.setMessage();
            if (this.authService.isLoggedIn) {
                this.redirectPage()
            }
        }, err => {
            console.log(err);
            this.message = err.error;
        });
    }

    redirectPage() {
        // get the redirect URL from our auth service
        // if no redirect has been set, use the default
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/home';

        // set our navigation extras object
        // that passes on our global query params and fragment
        let navigationExtras: NavigationExtras = {
            preserveQueryParams: true,
            preserveFragment: true
        };
        // redirect the user
        this.router.navigate([redirect], navigationExtras);
    }

}
