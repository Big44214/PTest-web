import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { UserService } from '../../core/services/user.service';
// Import BlockUI decorator & optional NgBlockUI type
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
    selector: 'app-first-page'
    , templateUrl: './first-page.component.html'
    , styleUrls: ['./first-page.component.scss']
})
export class FirstPageComponent implements OnInit {
    // Decorator wires up blockUI instance
    @BlockUI() blockUI: NgBlockUI;

    projectName = 'projectName';
    username: string = "";
    public disabled = false;
    public status: { isopen: boolean } = { isopen: false };

    constructor(public router: Router, private authService: AuthService, private userService: UserService) {
        this.username = userService.getUserName();
        this.blockUI.start(); // Start blocking
        this.blockUI.stop(); // Stop blocking

    }

    ngOnInit() { }

    public toggled(open: boolean): void {
        console.log('Dropdown is now: ', open);
    }

    public toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    onLoggedout() {
        this.authService.logout();
    }

}
