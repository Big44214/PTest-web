import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import * as remark from '../../core/interfaces/from-remark.interface';

const remark_Text = remark.text;

@Component({
    selector: 'app-forget-password'
    , templateUrl: './forget-password.component.html'
    , styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
    

    private form: FormGroup;

    constructor(private fb: FormBuilder, public router: Router) { }

    ngOnInit() {
        this.form = this.fb.group({
            'email': new FormControl('', [Validators.required,Validators.pattern(remark.text)])
        });
    }

    private onSave() {
 
    }

    private onClear() {
        this.form.reset();
    }

}
