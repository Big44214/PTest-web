import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-signup'
    , templateUrl: './signup.component.html'
    , styleUrls: ['./signup.component.scss']
})
export class SignUpComponent implements OnInit {

    projectName = 'projectName';

    constructor(public router: Router) { }

    ngOnInit() { }   

}
