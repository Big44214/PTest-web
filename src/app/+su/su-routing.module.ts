import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';
import { RoleGuard } from '../shared/guard/role.guard';
import { SuComponent } from './su.component';


import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FirstPageComponent } from './first-page/first-page.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

const routes: Routes = [{
    path: '', component: SuComponent
    , children: [{
        path: 'login'
        , component: LoginComponent
    }
    , {
        path: 'forgetpassword'
        , component: ForgetPasswordComponent
    }
    , {
        path: 'home'
        , component: FirstPageComponent
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin'], preload: true }
    }
    , {
        path: 'user'
        , children: [{
            path: ''
            , loadChildren: './su-rt/su-rt.module#SuRtModule'
            , data: { preload: true }
        }]
    }
    , {
        path: ''
        , redirectTo: 'login'
        , pathMatch: 'full'
    }, {
        path: '**'
        , component: NotFoundComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: []
})
export class SuRoutingModule { }
