import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SuRoutingModule } from './su-routing.module';
import { SuComponent } from './su.component';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FirstPageComponent } from './first-page/first-page.component';

@NgModule({
    imports: [
        SharedModule, SuRoutingModule
    ]
    , declarations: [
          SuComponent, LoginComponent, ForgetPasswordComponent, NotFoundComponent, FirstPageComponent
    ]
})
export class SuModule { }
