import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
// import {Message, SelectItem} from '../../../components/common/api';
import { SelectItem, InputMaskModule, PasswordModule} from 'primeng/primeng';
import * as remark from '../../../core/interfaces/from-remark.interface';

const remark_Text = remark.text;

@Component({
    selector: 'app-su-rt-rt01'
    , templateUrl: './su-rt-rt01.component.html'
    , styleUrls: ['./su-rt-rt01.component.scss']
})
export class SuRtRt01Component implements OnInit {

    projectName = 'projectName';
    private form: FormGroup;
    private role: SelectItem[];

    constructor(private fb: FormBuilder, public router: Router) { }

    ngOnInit() {
        this.form = this.fb.group({
            'userName': new FormControl('1', [Validators.required, Validators.pattern(remark_Text)]),
            'password': new FormControl('111111', Validators.required),
            'repassword': new FormControl('',Validators.required),
            'role': new FormControl('')
        });

        this.role = [];
        this.role.push({label:'Select Role', value:''});
        this.role.push({label:'Admin', value:'Admin'});
        this.role.push({label:'User', value:'User'});
    }

    private onSubmit() {
        console.info("==>" + this.form.valid);  
        // console.info(this.form.value);
    }

}
