import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service'
import { PanelModule } from 'primeng/primeng';

@Component({
    selector: 'app-su-rt-rt03'
    , templateUrl: './su-rt-rt03.component.html'
    , styleUrls: ['./su-rt-rt03.component.scss']
})
export class SuRtRt03Component implements OnInit {

    constructor(private authService: AuthService) { }

    ngOnInit() { }

    onLoggedout() {
        this.authService.logout();
    }

    onSave() {
        alert("แก้ไขสำเร็จ");
    }

}
