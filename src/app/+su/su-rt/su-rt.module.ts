import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SuRtRoutingModule } from './su-rt-routing.module';
import { SuRtComponent } from './su-rt.component';
import { SuRtRt01Component } from './su-rt-rt01/su-rt-rt01.component';
import { SuRtRt02Component } from './su-rt-rt02/su-rt-rt02.component';
import { SuRtRt03Component } from './su-rt-rt03/su-rt-rt03.component';
import { SuRtRt06Component } from './su-rt-rt06/su-rt-rt06.component';

export const _IMPORTS = [
    SharedModule, SuRtRoutingModule
]

export const _DECLARATIONS = [
    SuRtComponent, SuRtRt01Component, SuRtRt02Component, SuRtRt03Component, SuRtRt06Component
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class SuRtModule { }
