import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from '../../../shared/services/auth.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-su-rt-rt02'
    , templateUrl: './su-rt-rt02.component.html'
    , styleUrls: ['./su-rt-rt02.component.scss']
})
export class SuRtRt02Component implements OnInit {

    constructor() { }

    ngOnInit() { }

}
