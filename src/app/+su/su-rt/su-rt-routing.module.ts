import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/guard/auth.guard';
import { RoleGuard } from '../../shared/guard/role.guard';

import { SuRtComponent } from './su-rt.component';
import { SuRtRt01Component } from './su-rt-rt01/su-rt-rt01.component';
import { SuRtRt03Component } from './su-rt-rt03/su-rt-rt03.component';
import { SuRtRt06Component } from './su-rt-rt06/su-rt-rt06.component';

const routes: Routes = [{
    path: '', component: SuRtComponent
    , children: [{
        path: 'signup'
        , component: SuRtRt01Component
        , canActivate: [AuthGuard, RoleGuard]
        , data: { roles: ['admin'], preload: true }
    }, {
        path: 'manageprofile'
        , component: SuRtRt03Component
    }
    // ,{
    //     path: 'mangeuserprofile'
    //     , component: SuRtRt04Component
    // },{
    //     path: 'mangeaccessmenu'
    //     , component: SuRtRt05Component
    // }
    , {
        path: 'changepassword'
        , component: SuRtRt06Component
    }, {
        path: ''
        , redirectTo: 'manageprofile'
        , pathMatch: 'full'
    }
]
}];

@NgModule({
    imports: [ RouterModule.forChild(routes) ]
    , exports: [ RouterModule ]
    , providers: [ ]
})
export class SuRtRoutingModule { }
