import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuRtProfileComponent } from './su-rt-profile.component';
import { AuthGuard } from '../../../shared/guard/auth.guard';

const routes: Routes = [
    { path: '', component: SuRtProfileComponent
      , canActivateChild: [AuthGuard], data: { preload: true }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
  , exports: [RouterModule]
})
export class SuRtProfileRoutingModule { }
