import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { SuRtProfileRoutingModule } from './su-rt-profile-routing.module';
import { SuRtProfileComponent } from './su-rt-profile.component';

@NgModule({
    imports: [
        SharedModule, SuRtProfileRoutingModule
    ],
    declarations: [
        SuRtProfileComponent
    ]
})
export class SuRtProfileModule {
    
 }
