import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { InComponent } from './in.component';
import { InRoutingModule } from './in-routing.module';

@NgModule({
    imports: [
        SharedModule, InRoutingModule
    ]
    , declarations: [
        InComponent
    ]
})
export class InModule { }
