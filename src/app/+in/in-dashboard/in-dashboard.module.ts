import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { InDashboardRoutingModule } from './in-dashboard-routing.module';
import { InDashboardComponent } from './in-dashboard.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

@NgModule({
    imports: [
        SharedModule, InDashboardRoutingModule, Ng2Charts
    ],
    declarations: [
        InDashboardComponent
    ]
})
export class InDashboardModule { }
