import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AuthGuard } from '../../shared/guard/auth.guard';
import { InDashboardComponent } from './in-dashboard.component';

const routes: Routes = [
    { path: '', component: InDashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
  , exports: [RouterModule]
})
export class InDashboardRoutingModule { }
