import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'app-in-dashboard'
    , templateUrl: './in-dashboard.component.html'
    , styleUrls: ['./in-dashboard.component.scss']
})
export class InDashboardComponent implements OnInit {
    constructor(private translateService: TranslateService) {}
    ngOnInit() {}

    // bar chart
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;


    public barChartData: any[] = [
        {data: [65, 59, 80, 81, 56, 55, 40], label: this.translateService.instant('receiving')}
        , {data: [28, 48, 40, 19, 86, 27, 50], label: 'นำออก'}
        , {data: [10, 5, 11, 4, 2, 9, 16], label: 'แลกเปลี่ยน'}
    ];
    // Doughnut
    public doughnutChartLabels: string[] = ['นำเข้า', 'นำออก', 'แลกเปลี่ยน'];
    public doughnutChartData: number[] = [350, 450, 100];
    public doughnutChartType: string = 'doughnut';
  
    public lineChartColors: Array<any> = [
    { // grey
        backgroundColor: 'rgba(148,159,177,0.2)'
        , borderColor: 'rgba(148,159,177,1)'
        , pointBackgroundColor: 'rgba(148,159,177,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)'
        , borderColor: 'rgba(77,83,96,1)'
        , pointBackgroundColor: 'rgba(77,83,96,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
        backgroundColor: 'rgba(148,159,177,0.2)'
        , borderColor: 'rgba(148,159,177,1)'
        , pointBackgroundColor: 'rgba(148,159,177,1)'
        , pointBorderColor: '#fff'
        , pointHoverBackgroundColor: '#fff'
        , pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }

    public randomize(): void {
        // Only Change 3 values
        const data = [
            Math.round(Math.random() * 100)
            , 59
            , 80
            , (Math.random() * 100)
            , 56
            , (Math.random() * 100)
            , 40
        ];
        const clone = JSON.parse(JSON.stringify(this.barChartData));
        clone[0].data = data;
        this.barChartData = clone;
        /**
        * (My guess), for Angular to recognize the change in the dataset
        * it has to change the dataset variable directly,
        * so one way around it, is to clone the data, change it and then
        * assign it;
        */
    }

}
