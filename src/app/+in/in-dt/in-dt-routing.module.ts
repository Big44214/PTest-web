import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InDtComponent } from './in-dt.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

//Component.
import { InDtDashboardComponent } from './in-dt-dashboard/in-dt-dashboard.component';
import { InDtDt01Component } from './in-dt-dt01/in-dt-dt01.component';
import { InDtDt02Component } from './in-dt-dt02/in-dt-dt02.component';
import { InDtDt03Component } from './in-dt-dt03/in-dt-dt03.component';
import { InDtDt11Component } from './in-dt-dt11/in-dt-dt11.component';
import { InDtDt12Component } from './in-dt-dt12/in-dt-dt12.component';
import { InDtDt13Component } from './in-dt-dt13/in-dt-dt13.component';
import { InDtDt21Component } from './in-dt-dt21/in-dt-dt21.component';
import { InDtDt22Component } from './in-dt-dt22/in-dt-dt22.component';
import { InDtDt23Component } from './in-dt-dt23/in-dt-dt23.component';

//Resolve.
import { InDtDt01Resolve } from './in-dt-dt01/in-dt-dt01.resolve';
import { InDtDt02Resolve } from './in-dt-dt02/in-dt-dt02.resolve';
import { InDtDt03Resolve } from './in-dt-dt03/in-dt-dt03.resolve';
import { InDtDt11Resolve } from './in-dt-dt11/in-dt-dt11.resolve';
import { InDtDt12Resolve } from './in-dt-dt12/in-dt-dt12.resolve';
import { InDtDt13Resolve } from './in-dt-dt13/in-dt-dt13.resolve';
import { InDtDt21Resolve } from './in-dt-dt21/in-dt-dt21.resolve';
import { InDtDt22Resolve } from './in-dt-dt22/in-dt-dt22.resolve';
import { InDtDt23Resolve } from './in-dt-dt23/in-dt-dt23.resolve';

//Service.
import { InDtDt01Service } from './in-dt-dt01/in-dt-dt01.service';
import { InDtDt02Service } from './in-dt-dt02/in-dt-dt02.service';
import { InDtDt03Service } from './in-dt-dt03/in-dt-dt03.service';
import { InDtDt11Service } from './in-dt-dt11/in-dt-dt11.service';
import { InDtDt12Service } from './in-dt-dt12/in-dt-dt12.service';
import { InDtDt13Service } from './in-dt-dt13/in-dt-dt13.service';
import { InDtDt21Service } from './in-dt-dt21/in-dt-dt21.service';
import { InDtDt22Service } from './in-dt-dt22/in-dt-dt22.service';
import { InDtDt23Service } from './in-dt-dt23/in-dt-dt23.service';

const routes: Routes = [{
    path: '', component: InDtComponent,
    children: [{
        path: 'dashboard'
        , component: InDtDashboardComponent
        , canActivateChild: [AuthGuard]
    }, {
        path: 'receiving/:refresh'
        , component: InDtDt01Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt01Resolve }
    }, {
        path: 'saling/:refresh'
        , component: InDtDt02Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt02Resolve }
    }, {
        path: 'adjusting/:refresh'
        , component: InDtDt03Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt03Resolve }
    }, {
        path: 'receivefrombuy/:refresh'
        , component: InDtDt11Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt11Resolve }
    }, {
        path: 'receivefromcustomer/:refresh'
        , component: InDtDt12Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt12Resolve }
    }, {
        path: 'sitetransferfrom/:refresh'
        , component: InDtDt13Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt13Resolve }
    }, {
        path: 'sale/:refresh'
        , component: InDtDt21Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt21Resolve }
    }, {
        path: 'returntovendor/:refresh'
        , component: InDtDt22Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt22Resolve }
    }, {
        path: 'sitetransferinto/:refresh'
        , component: InDtDt23Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InDtDt23Resolve }
    }, {
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [
          InDtDt01Resolve, InDtDt01Service
        , InDtDt02Resolve, InDtDt02Service
        , InDtDt03Resolve, InDtDt03Service
        , InDtDt11Resolve, InDtDt11Service
        , InDtDt12Resolve, InDtDt12Service
        , InDtDt13Resolve, InDtDt13Service
        , InDtDt21Resolve, InDtDt21Service
        , InDtDt22Resolve, InDtDt22Service
        , InDtDt23Resolve, InDtDt23Service
        // , InDtDt04Resolve, InDtDt04Service
        // , InDtDt05Resolve, InDtDt05Service 
    ]
})
export class InDtRoutingModule { }
