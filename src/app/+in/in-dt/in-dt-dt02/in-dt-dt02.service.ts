import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared/services/http-service.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InDtDt02Service {

  constructor(public httpService: HttpService) { }

  onSave(method: String, data: Object) {
    return this.httpService.save(method, data);
  }

  onSearch(method: String, data: Object) {
    return this.httpService.search(method, data);
  }

  private isObservableEmpty(value: any): boolean {
    return (value.source && value.operator) ? false : true;
  }

  private _dt02Combobox: Observable<any> = new Observable<any>();
  get dt02Combobox(): any { return this._dt02Combobox }
  set dt02Combobox(value: any) { this._dt02Combobox = value; }
  public getdt02Combobox(): Observable<any> {
    return this.isObservableEmpty(this.dt02Combobox) ? this.httpService.search('dt02Combo', {}) : this.dt02Combobox;
  }

}