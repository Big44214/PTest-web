export interface tranDetail {
    transdtlId?;
    statusDtl?;
    productId?;
    productDetail?;
    productUms?;
    transQty?;
    transCost?;
    transAmount?;
}