import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { InDtDt02Service } from './in-dt-dt02.service';
import { SelectItem } from 'primeng/primeng';
import { tranDetail } from './tran-detail.interface';

@Component({
    selector: 'app-in-dt-dt02'
    , templateUrl: './in-dt-dt02.component.html'
    , styleUrls: ['./in-dt-dt02.component.scss']
})
export class InDtDt02Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private modalOptions: NgbModalOptions = { backdrop: 'static', size: 'lg' }
    private recordsSearch: any[];
    private docTypeCode: SelectItem[];
    private transStatus: SelectItem[];
    private warehouseId: SelectItem[];
    private warehouseTemId: SelectItem[];
    private customerId: SelectItem[];
    private searchModal: boolean = false;
    private tranDetail: tranDetail[];
    private newTranDetail: tranDetail = new newTranDetail();
    private product: SelectItem[];

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal, private inDtDt02Service: InDtDt02Service) {

        this.form = fb.group({
            refNo: fb.control("")
            , transNote: fb.control("")
            , transStatus: fb.control(1)
            , docTypeCode: fb.control("", Validators.required)
            , warehouseId: fb.control(null, Validators.required)
            , warehouseTemId: fb.control(null)
            , dateRef: fb.control("")
            , customerId: fb.control(null)
            , supplierId: fb.control(null)
            , transNo: fb.control("")
            , transDate: fb.control("")
            , inTransactionDtl: fb.control("")
            , statusHdr: fb.control("")
            , transhdrId: fb.control(null)
        });

        this.formSearchModal = fb.group({
            transhdrId: fb.control("", Validators.required)
        });
    }

    ngOnInit() {
        // this.docTypeCode = [];
        // this.docTypeCode.push({label:'ขายสินค้า', value:1});
        // this.docTypeCode.push({label:'ส่งคืนผู้จำหน่าย', value:2});
        // this.docTypeCode.push({label:'เบิกเพื่อโอนสินค้าออกจากคลัง', value:3});

        this.transStatus = [];
        this.transStatus.push({ label: 'ปกติ', value: 1 });
        this.transStatus.push({ label: 'ยกเลิก', value: 0 });


        // this.warehouseId = [];
        // this.warehouseId.push({label:'กรุงเทพฯ', value:1});
        // this.warehouseId.push({label:'เชียงราย', value:2});

        this.warehouseTemId = [];
        this.warehouseTemId.push({ label: 'กรุงเทพฯ', value: 1 });
        this.warehouseTemId.push({ label: 'เชียงราย', value: 2 });

        // this.customerId = [];
        // this.customerId.push({label:'นาย อ้น', value:1});
        // this.customerId.push({label:'นาย ก่อน', value:2});

        this.tranDetail = [{
            'transdtlId': '1'
            , 'productId': 'A001'
            , 'statusDtl': ''
            , 'productDetail': 'ทองแท่ง ปั๊มโลโก้ 96.5%'
            , 'transQty': '10'
        }
        ];

        this.product = [];
        this.product.push({ label: 'ทองแท่ง', value: 'ทองแท่ง' });
        this.product.push({ label: 'สร้อยคอทองคำ', value: 'สร้อยคอทองคำ' });

        // this.onSearch();
    }

    onClear() {
        this.form.reset();
        this.form.controls['itemType'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "item": this.form.value
        };
        this.inDtDt02Service.onSave('saveItem', dataToSave).subscribe(
            data => {
                // console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inDtDt02Service.onSearch('dt02Combo', {}).subscribe(
            data => {
                this.warehouseId = data.json().warehouse.data
                this.docTypeCode = data.json().doctype.data
                this.customerId = data.json().customer.data
            },
            err => console.info(err));
    }


    private open(content) {
        this.modalService.open(content, this.modalOptions)
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }

    private onRowSelect(event) {
        // console.info(event);
    }

    addRow() {
        if (this.form.valid) {
            this.newTranDetail = new newTranDetail();
            this.newTranDetail.productId = 'กรุณาเลือกสินค้า';
            let tranDetail = [...this.tranDetail];
            tranDetail.push(this.newTranDetail);
            this.tranDetail = tranDetail;
        } else {
        }
    }

    deleteRow(index) {
        // console.info(index);
        this.tranDetail = this.tranDetail.filter((val, i) => i != index);
        this.newTranDetail = null;
    }

    itemChange(event, index) {
        if (event.value == 'ทองแท่ง') {
            this.tranDetail.filter((val, i) => i == index)[0].productDetail = 'ทองแท่ง ปั๊มโลโก้ 96.5%';
        }
        else if (event.value == 'สร้อยคอทองคำ') {
            this.tranDetail.filter((val, i) => i == index)[0].productDetail = 'สร้อยคอทองคำ ลายมังกร 99.9%';
        }
    }
}

class newTranDetail implements tranDetail {
    constructor(public transdtlId?, public statusDtl?, public productId?, public productDetail?
        , public productUms?, public transQty?, public transCost?, public transAmount?) { }
}
