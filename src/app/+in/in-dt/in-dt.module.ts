import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { InDtComponent } from './in-dt.component';
import { InDtRoutingModule } from './in-dt-routing.module';

//Dt Dashboard.
import { InDtDashboardComponent } from './in-dt-dashboard/in-dt-dashboard.component';

//Dt01.
import { InDtDt01Component } from './in-dt-dt01/in-dt-dt01.component';

//Dt02.
import { InDtDt02Component } from './in-dt-dt02/in-dt-dt02.component';

//Dt03.
import { InDtDt03Component } from './in-dt-dt03/in-dt-dt03.component';

//Dt011.
import { InDtDt11Component } from './in-dt-dt11/in-dt-dt11.component';

//Dt012.
import { InDtDt12Component } from './in-dt-dt12/in-dt-dt12.component';

//Dt013.
import { InDtDt13Component } from './in-dt-dt13/in-dt-dt13.component';

//Dt21.
import { InDtDt21Component } from './in-dt-dt21/in-dt-dt21.component';

//Dt22.
import { InDtDt22Component } from './in-dt-dt22/in-dt-dt22.component';

//Dt21.
import { InDtDt23Component } from './in-dt-dt23/in-dt-dt23.component';

export const _IMPORTS = [
    SharedModule, InDtRoutingModule
]

export const _DECLARATIONS = [
    InDtComponent
    , InDtDashboardComponent
    , InDtDt01Component
    , InDtDt02Component
    , InDtDt03Component
    , InDtDt11Component
    , InDtDt12Component
    , InDtDt13Component
    , InDtDt21Component
    , InDtDt22Component
    , InDtDt23Component
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class InDtModule { }
