import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared/services/http-service.service';
import { Router } from '@angular/router';

@Injectable()
export class InDtDt13Service {

    constructor(public httpService: HttpService) { }

    onSave(method: String, data: Object) {
        return this.httpService.save(method, data);
    }

    onSearch(method: String, data: Object) {
        return this.httpService.search(method, data);
    }

}