import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { InDtDt03Service } from './in-dt-dt03.service';

@Injectable()
export class InDtDt03Resolve implements Resolve<any> {

    constructor(private inDtDt03Service: InDtDt03Service){ }

    resolve(route: ActivatedRouteSnapshot): any {
        if(route.url[0].path == 'item'){
            console.info("info:  "+route.url[0].path);
            // this.inRtRt05Service.onSearch();
        }
    }
}