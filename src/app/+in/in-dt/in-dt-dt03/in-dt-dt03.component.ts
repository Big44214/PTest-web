import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModal, ModalDismissReasons,NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { InDtDt03Service } from './in-dt-dt03.service';
import {SelectItem} from 'primeng/primeng';
import {tranDetail} from './tran-detail.interface';

@Component({
    selector: 'app-in-dt-dt03'
    , templateUrl: './in-dt-dt03.component.html'
    , styleUrls: ['./in-dt-dt03.component.scss']
})
export class InDtDt03Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private recordsSearch: any[];
    private docType: SelectItem[];
    private transStatus: SelectItem[];
    private warehouse: SelectItem[];
    private customer: SelectItem[];
    private product: SelectItem[];
    private tranDetail:tranDetail[];
    private newTranDetail: tranDetail = new newTranDetail();

    constructor(@Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal, private inDtDt03Service: InDtDt03Service) {

        this.form = fb.group({
              transNote:  fb.control("")
            , transStatus:  fb.control(1)
            , docTypeCode:  fb.control(31, Validators.required)
            , warehouseId:  fb.control(null, Validators.required)
            , dateRef:  fb.control("")
            , transNo:  fb.control("")
            , transDate:  fb.control("")
            , inTransactionDtl:  fb.control("")
            , statusHdr:  fb.control("")
            , refNo:  fb.control("")
        });

        this.formSearchModal = fb.group({
            itemCd:  fb.control("", Validators.required)
        });
    }

 ngOnInit() {
        this.docType = [];
        this.docType.push({label:'รายการรับสินค้า', value:1});
        this.docType.push({label:'รายการจ่ายสินค้า', value:2});
        this.docType.push({label:'รายการปรับปรุงสินค้า', value:3});

        this.transStatus = [];
        this.transStatus.push({label:'ปกติ', value:1});
        this.transStatus.push({label:'ยกเลิก', value:0});

        this.warehouse = [];
        this.warehouse.push({label:'กรุงเทพฯ', value:1});
        this.warehouse.push({label:'เชียงราย', value:2});

        this.customer = [];
        this.customer.push({label:'นาย อ้น', value:1});
        this.customer.push({label:'นาย ก่อน', value:2});

        this.product = [];
        this.product.push({label:'ทองแท่ง', value:'ทองแท่ง'});
        this.product.push({label:'สร้อยคอทองคำ', value:'สร้อยคอทองคำ'});

        this.tranDetail = [{
            'transdtlId': '1'
            , 'productId': 'ทองแท่ง'
            , 'statusDtl': ''
            , 'productDetail': 'ทองแท่ง ปั๊มโลโก้ 96.5%'
            , 'transQty': '10'
            , 'transCost': '100'
            , 'transAmount': '1000'
        }
        ];
        
        this.form.controls['refNo'].setValue('foo bar');
        
    }

    onClear() {
        this.form.reset();
        this.form.controls['itemType'].setValue('');
    }

    addRow() {
        this.newTranDetail = new newTranDetail();
        this.newTranDetail.productId = 'กรุณาเลือกสินค้า';
        let tranDetail = [...this.tranDetail];
        tranDetail.push(this.newTranDetail);
        this.tranDetail = tranDetail;
    }

    deleteRow(index) {
        console.info(index);
        this.tranDetail = this.tranDetail.filter((val,i) => i!=index);
        this.newTranDetail = null;
    }

    itemChange(event,index){
        if(event.value == 'ทองแท่ง'){
            this.tranDetail.filter((val,i) => i==index)[0].productDetail = 'ทองแท่ง ปั๊มโลโก้ 96.5%';
        }
        else if(event.value == 'สร้อยคอทองคำ'){
            this.tranDetail.filter((val,i) => i==index)[0].productDetail = 'สร้อยคอทองคำ ลายมังกร 99.9%';
        }
    }
}

class newTranDetail implements tranDetail {
    constructor(public transdtlId?, public statusDtl?, public productId?, public productDetail?
    , public productUms?, public transQty?, public transCost?, public transAmount?) {}
}