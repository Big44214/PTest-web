import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { InDtDt21Service } from './in-dt-dt21.service';
import { SelectItem } from 'primeng/primeng';
import { tranDetail } from './tran-detail.interface';


@Component({
    selector: 'app-in-dt-dt21'
    , templateUrl: './in-dt-dt21.component.html'
    , styleUrls: ['./in-dt-dt21.component.scss']
})
export class InDtDt21Component implements OnInit {
    alertModalToggle(arg0: any, arg1: any): any {
        throw new Error("Method not implemented.");
    }

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private recordsSearch: any[];
    private docType: SelectItem[];
    private transStatus: SelectItem[];
    private warehouse: SelectItem[];
    private customer: SelectItem[];
    private product: SelectItem[];
    private tranDetail: tranDetail[];
    private newTranDetail: tranDetail = new newTranDetail();

    private selectedTranDetail: tranDetail;


    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private inDtDt21Service: InDtDt21Service) {

        this.form = fb.group({
            refNo: fb.control("")
            , transNote: fb.control("")
            , transStatus: fb.control(1)
            , docTypeCode: fb.control(null, Validators.required)
            , warehouseId: fb.control(null, Validators.required)
            , warehouseTemId: fb.control(null)
            , dateRef: fb.control("")
            , customerId: fb.control(null)
            , supplierId: fb.control(null)
            , transNo: fb.control("")
            , transDate: fb.control("")
            , inTransactionDtl: fb.control("")
            , statusHdr: fb.control("")
            , transhdrId: fb.control(null)
        });

        this.formSearchModal = fb.group({
            itemCd: fb.control("", Validators.required)
        });
    }

    ngOnInit() {
        this.docType = [];
        this.docType.push({ label: 'รายการรับสินค้า', value: 1 });
        this.docType.push({ label: 'รายการจ่ายสินค้า', value: 2 });
        this.docType.push({ label: 'รายการปรับปรุงสินค้า', value: 3 });

        this.transStatus = [];
        this.transStatus.push({ label: 'ปกติ', value: 1 });
        this.transStatus.push({ label: 'ยกเลิก', value: 0 });

        this.warehouse = [];
        this.warehouse.push({ label: 'กรุงเทพฯ', value: 1 });
        this.warehouse.push({ label: 'เชียงราย', value: 2 });

        this.customer = [];
        this.customer.push({ label: 'นาย อ้น', value: 1 });
        this.customer.push({ label: 'นาย ก่อน', value: 2 });

        this.product = [];
        this.product.push({ label: 'ทองแท่ง', value: 'ทองแท่ง' });
        this.product.push({ label: 'สร้อยคอทองคำ', value: 'สร้อยคอทองคำ' });

        this.tranDetail = [{
            'transdtlId': '1'
            , 'productId': 'ทองแท่ง'
            , 'statusDtl': ''
            , 'productDetail': 'ทองแท่ง ปั๊มโลโก้ 96.5%'
            , 'transQty': '10'
            , 'transCost': '100'
            , 'transAmount': '1000'
        }
        ];
    }

    onClear() {
        this.form.reset();
        this.form.controls['itemType'].setValue('');
    }

    addRow() {
        if (this.form.valid == true && this.form.valid == true) {
            this.newTranDetail = new newTranDetail();
            this.newTranDetail.productId = 'กรุณาเลือกสินค้า';
            let tranDetail = [...this.tranDetail];
            tranDetail.push(this.newTranDetail);
            this.tranDetail = tranDetail;
        } else {
            console.info("11111111");
        }
    }

    deleteRow(index) {
        console.info(index);
        this.tranDetail = this.tranDetail.filter((val, i) => i != index);
        this.newTranDetail = null;
    }

    itemChange(event, index) {
        if (event.value == 'ทองแท่ง') {
            this.tranDetail.filter((val, i) => i == index)[0].productDetail = 'ทองแท่ง ปั๊มโลโก้ 96.5%';
        }
        else if (event.value == 'สร้อยคอทองคำ') {
            this.tranDetail.filter((val, i) => i == index)[0].productDetail = 'สร้อยคอทองคำ ลายมังกร 99.9%';
        }
    }
}

class newTranDetail implements tranDetail {
    constructor(public transdtlId?, public statusDtl?, public productId?, public productDetail?
        , public productUms?, public transQty?, public transCost?, public transAmount?) { }
}