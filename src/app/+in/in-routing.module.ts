import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';
import { InComponent } from './in.component';


const routes: Routes = [{
    path: '', component: InComponent,
    children: [
        {
            path: 'dashboard'
            , loadChildren: './in-dashboard/in-dashboard.module#InDashboardModule'
            , canActivateChild: [AuthGuard]
        }
        , {
            path: 'daily'
            , loadChildren: './in-dt/in-dt.module#InDtModule'
            , canActivateChild: [AuthGuard]
        }
        , {
            path: 'master'
            , loadChildren: './in-rt/in-rt.module#InRtModule'
            , canActivateChild: [AuthGuard]
        }
        , {
            path: ''
            , redirectTo: 'dashboard'
            , pathMatch: 'full'
        }
    ]
}
];

@NgModule({
      imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
})
export class InRoutingModule { }
