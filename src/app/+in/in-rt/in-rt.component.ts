import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-in-rt'
    , templateUrl: './in-rt.component.html'
})
export class InRtComponent implements OnInit {

    constructor(public router: Router) { }

    ngOnInit() { }

}
