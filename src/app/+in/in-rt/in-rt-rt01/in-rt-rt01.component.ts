import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { InRtRt01Service } from './in-rt-rt01.service';
import { SelectItem, InputMaskModule } from 'primeng/primeng';
import * as remark from '../../../core/interfaces/from-remark.interface';

const remark_Email = remark.email;
const remark_ddmmyyyy = remark.ddmmyyyy;
const remark__Tel = remark.telephone;
const remark__Tax = remark.tax;
const remark__Fax = remark.fax;

@Component({
    selector: 'app-in-rt-rt01'
    , templateUrl: './in-rt-rt01.component.html'
    , styleUrls: ['./in-rt-rt01.component.scss']
})
export class InRtRt01Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private modalOptions: NgbModalOptions = { backdrop: 'static', size: 'lg' }
    private recordsSearch: any[];
    private vendorStatus: SelectItem[];
    private vendorUnitType: SelectItem[];
    private vendorWeigthType: SelectItem[];
    private searchModal: boolean = false;

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal
        , private inRtRt01Service: InRtRt01Service) {

        this.form = fb.group({
            vendorCd: fb.control("", Validators.required),
            vendorName: fb.control(""),
            vendorContact: fb.control(""),
            vendorTel: fb.control("", Validators.pattern(remark__Tel)),
            vendorEmail: fb.control("", Validators.pattern(remark_Email)),
            vendorStatus: fb.control("1", Validators.required),
            vendorAddress: fb.control(""),
            vendorFax: fb.control("", Validators.pattern(remark__Fax)),
            vendorTaxCode: fb.control("", Validators.pattern(remark__Tax)),
            vendorWorkPlace: fb.control(""),
            vendorCredit: fb.control("", Validators.pattern(remark_ddmmyyyy))
        });

        this.formSearchModal = fb.group({
            vendorCd: fb.control("", Validators.required)
        });
    }
    ngOnInit() {

        this.vendorStatus = [];
        this.vendorStatus.push({ label: 'ใช้', value: 1 });
        this.vendorStatus.push({ label: 'ไม่ใช้', value: 2 });

        this.recordsSearch = [{
            'vendorCd': 'V001',
            'vendorName': 'วรัญญากร สีขาว',
            'vendorContact': 'ชวนิต วิริยะวิสุทธิสกุล',
            'vendorTel': '0987654321',
            'vendorEmail': 'winZaza@gmail.com'
        }
            , {
            'vendorCd': 'V002',
            'vendorName': 'ชวนิต วิริยะวิสุทธิสกุล',
            'vendorContact': 'วรัญญากร สีขาว',
            'vendorTel': '0123456789',
            'vendorEmail': 'PangluvP@gmail.com'
        }];
    }

    onClear() {
        this.form.reset();
        this.form.controls['vendorType'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "vendor": this.form.value
        };
        this.inRtRt01Service.onSave('saveVendor', dataToSave).subscribe(
            data => {
                console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inRtRt01Service.onSearch('searchVendor', {}).subscribe(
            data => {
                this.recordsSearch = data.json().data.vendor
            },
            err => console.info(err));
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }

    private onSubmit() {
        console.info("==>" + this.form.valid);
    }
}