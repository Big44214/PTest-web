import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { InRtRt03Service } from './in-rt-rt03.service';
import { SelectItem } from 'primeng/primeng';

@Component({
    selector: 'app-in-rt-rt03'
    , templateUrl: './in-rt-rt03.component.html'
    , styleUrls: ['./in-rt-rt03.component.scss']
})
export class InRtRt03Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private modalOptions: NgbModalOptions = { backdrop: 'static', size: 'lg' }
    private recordsSearch: any[];
    private warehouseStatus: SelectItem[];
    private warehouseUnitType: SelectItem[];
    private warehouseWeigthType: SelectItem[];
    private searchModal: boolean = false;

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal
        , private inRtRt03Service: InRtRt03Service) {

        this.form = fb.group({
            warehouseCd: fb.control(""),
            warehouseName: fb.control("", Validators.required),
            warehouseStatus: fb.control("1", Validators.required),
        });
        this.formSearchModal = fb.group({
            warehouseCd: fb.control("", Validators.required)
        });
    }

    ngOnInit() {
        this.warehouseStatus = [];
        this.warehouseStatus.push({ label: 'ใช้', value: 1 });
        this.warehouseStatus.push({ label: 'ไม่ใช้', value: 2 });

        this.recordsSearch = [{
            'warehouseCd': 'WH001',
            'warehouseName': 'White Colour'
        }
            , {
            'warehouseCd': 'WH002',
            'warehouseName': 'Glasses'
        }];
    }
    onClear() {
        this.form.reset();
        this.form.controls['warehouseType'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "warehouse": this.form.value
        };
        this.inRtRt03Service.onSave('saveWarehouse', dataToSave).subscribe(
            data => {
                console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inRtRt03Service.onSearch('searchWarehouse', {}).subscribe(
            data => {
                this.recordsSearch = data.json().data.warehouse
            },
            err => console.info(err));
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }
}