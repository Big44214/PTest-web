import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { InRtRt05Service } from './in-rt-rt05.service';
import { SelectItem } from 'primeng/primeng';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryImageSize } from 'ngx-gallery';
@Component({
    selector: 'app-in-rt-rt05'
    , templateUrl: './in-rt-rt05.component.html'
    , styleUrls: ['./in-rt-rt05.component.scss']
})
export class InRtRt05Component implements OnInit {
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private recordsSearch: any[];
    private itemType: SelectItem[];
    private itemUnitType: SelectItem[];
    private itemWeigthType: SelectItem[];
    private searchModal: boolean = false;
    images: any[];

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private inRtRt05Service: InRtRt05Service) {

        this.form = fb.group({
            itemCd: fb.control(""),
            itemName: fb.control("", Validators.required),
            itemDesc: fb.control("", Validators.required),
            itemType: fb.control("", Validators.required),
            itemPrice: fb.control("", Validators.required),
            itemWeigth: fb.control("", Validators.required),
            itemOrder: fb.control("", Validators.required),
            itemUnit: fb.control("", Validators.required),
            itemUnitType: fb.control("", Validators.required),
            itemWeigthType: fb.control("", Validators.required)
        });

        this.formSearchModal = fb.group({
            itemCd: fb.control("", Validators.required)
        });
    }

    ngOnInit() {

        this.galleryOptions = [
            {
                width: "103%",
                height: "333px",
                imagePercent: 100,
                thumbnailsPercent: 20,
                thumbnailsColumns: 6,
                thumbnailsMargin: 0,
                thumbnailMargin: 0,
                thumbnailsSwipe: true,
                imageAnimation: NgxGalleryAnimation.Slide

            },

            {
                breakpoint: 1024,
                width: "103%",
                height: "322px",
                thumbnailsColumns: 6,
                thumbnailsPercent: 20,
                thumbnailsSwipe: true,
            },

            {
                breakpoint: 704,

                width: "100%",
                thumbnailsMargin: 0,
                height: "300px",
                thumbnailsColumns: 5,
                thumbnailsPercent: 20,
                preview: false,
                thumbnailsSwipe: true,

            }
        ];

        this.galleryImages = [
            {
                small: 'assets/img/avatars/tong1.jpg',
                medium: 'assets/img/avatars/tong1.jpg',
                big: 'assets/img/avatars/tong1.jpg'
            },
            {
                small: 'assets/img/avatars/tong2.jpg',
                medium: 'assets/img/avatars/tong2.jpg',
                big: 'assets/img/avatars/tong2.jpg',
            },
            {
                small: 'assets/img/avatars/tong3.jpg',
                medium: 'assets/img/avatars/tong3.jpg',
                big: 'assets/img/avatars/tong3.jpg'
            },
            {
                small: 'assets/img/avatars/tong4.jpg',
                medium: 'assets/img/avatars/tong4.jpg',
                big: 'assets/img/avatars/tong4.jpg'
            },
            {
                small: 'assets/img/avatars/tong5.jpg',
                medium: 'assets/img/avatars/tong5.jpg',
                big: 'assets/img/avatars/tong5.jpg'
            },
            {
                small: 'assets/img/avatars/tong6.jpg',
                medium: 'assets/img/avatars/tong6.jpg',
                big: 'assets/img/avatars/tong6.jpg'
            }
        ];

        this.itemType = [];
        this.itemType.push({ label: 'ทองคำแท่ง', value: 1 });
        this.itemType.push({ label: 'ทองรูปพรรณ', value: 2 });

        this.itemUnitType = [];
        this.itemUnitType.push({ label: 'เส้น', value: 1 });
        this.itemUnitType.push({ label: 'วง', value: 2 });
        this.itemUnitType.push({ label: 'อื่นๆ', value: 3 });

        this.itemWeigthType = [];
        this.itemWeigthType.push({ label: '1 สลึง', value: 1 });
        this.itemWeigthType.push({ label: '2 สลึง', value: 2 });
        this.itemWeigthType.push({ label: '1 บาท', value: 3 });
        this.itemWeigthType.push({ label: '5 บาท', value: 4 });
        this.itemWeigthType.push({ label: '10 บาท', value: 5 });

        this.recordsSearch = [{
            'itemCd': 'A001'
            , 'itemName': 'ทองคำแท่ง'
            , 'itemDesc': 'ทองแท่ง ปั๊มโลโก้ 96.5%'
        }
            , {
            'itemCd': 'A002'
            , 'itemName': 'ทองคำแท่ง'
            , 'itemDesc': 'ทองแท่ง ปั๊มโลโก้ 99.9%'
        }];
    }

    onClear() {
        this.form.reset();
        this.form.controls['itemType'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "item": this.form.value
        };
        this.inRtRt05Service.onSave('saveItem', dataToSave).subscribe(
            data => {
                console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inRtRt05Service.onSearch('searchItem', {}).subscribe(
            data => {
                this.recordsSearch = data.json().data.item
            },
            err => console.info(err));
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }

    private onRowSelect(event) {
        console.info(event);
    }

}