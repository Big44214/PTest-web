import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { InRtRt05Service } from './in-rt-rt05.service';

@Injectable()
export class InRtRt05Resolve implements Resolve<any> {

    constructor(private inRtRt05Service: InRtRt05Service) { }

    resolve(route: ActivatedRouteSnapshot): any {
        if (route.url[0].path == 'item') {
            console.info("info:  " + route.url[0].path);
            // this.inRtRt05Service.onSearch();
        }
    }
}