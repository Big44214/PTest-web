import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { InRtComponent } from './in-rt.component';
import { InRtRoutingModule } from './in-rt-routing.module';

//Rt Dashboard.
import { InRtDashboardComponent } from './in-rt-dashboard/in-rt-dashboard.component';

//Rt01.
import { InRtRt01Component } from './in-rt-rt01/in-rt-rt01.component';

//Rt02.
import { InRtRt02Component } from './in-rt-rt02/in-rt-rt02.component';

//Rt03.
import { InRtRt03Component } from './in-rt-rt03/in-rt-rt03.component';

//Rt04.
import { InRtRt04Component } from './in-rt-rt04/in-rt-rt04.component';

//Rt05.
import { InRtRt05Component } from './in-rt-rt05/in-rt-rt05.component';

export const _IMPORTS = [
    SharedModule, InRtRoutingModule
]

export const _DECLARATIONS = [
    InRtComponent, InRtDashboardComponent, InRtRt01Component, InRtRt02Component, InRtRt03Component
    , InRtRt04Component, InRtRt05Component
]

@NgModule({
    imports: _IMPORTS
    , declarations: _DECLARATIONS
})
export class InRtModule { }
