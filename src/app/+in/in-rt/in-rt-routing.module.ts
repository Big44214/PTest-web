import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InRtComponent } from './in-rt.component';
import { AuthGuard } from '../../shared/guard/auth.guard';

//Component.
import { InRtDashboardComponent } from './in-rt-dashboard/in-rt-dashboard.component';
import { InRtRt01Component } from './in-rt-rt01/in-rt-rt01.component';
import { InRtRt02Component } from './in-rt-rt02/in-rt-rt02.component';
import { InRtRt03Component } from './in-rt-rt03/in-rt-rt03.component';
import { InRtRt04Component } from './in-rt-rt04/in-rt-rt04.component';
import { InRtRt05Component } from './in-rt-rt05/in-rt-rt05.component';

//Resolve.
import { InRtRt01Resolve } from './in-rt-rt01/in-rt-rt01.resolve';
import { InRtRt02Resolve } from './in-rt-rt02/in-rt-rt02.resolve';
import { InRtRt03Resolve } from './in-rt-rt03/in-rt-rt03.resolve';
import { InRtRt04Resolve } from './in-rt-rt04/in-rt-rt04.resolve';
import { InRtRt05Resolve } from './in-rt-rt05/in-rt-rt05.resolve';

//Service.
import { InRtRt01Service } from './in-rt-rt01/in-rt-rt01.service';
import { InRtRt02Service } from './in-rt-rt02/in-rt-rt02.service';
import { InRtRt03Service } from './in-rt-rt03/in-rt-rt03.service';
import { InRtRt04Service } from './in-rt-rt04/in-rt-rt04.service';
import { InRtRt05Service } from './in-rt-rt05/in-rt-rt05.service';

const routes: Routes = [{
    path: '', component: InRtComponent,
    children: [{
        path: 'dashboard'
        , component: InRtDashboardComponent
        , canActivateChild: [AuthGuard]
    }, {
        path: 'vendor/:refresh'
        , component: InRtRt01Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InRtRt01Resolve }
    }, {
        path: 'customer/:refresh'
        , component: InRtRt02Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InRtRt02Resolve }
    }, {
        path: 'warehouse/:refresh'
        , component: InRtRt03Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InRtRt03Resolve }
    }, {
        path: 'itemtype/:refresh'
        , component: InRtRt04Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InRtRt04Resolve }
    }, {
        path: 'item/:refresh'
        , component: InRtRt05Component
        , canActivateChild: [AuthGuard]
        , resolve: { fromLoadObject: InRtRt05Resolve }
    }, {
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
    }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
    , exports: [RouterModule]
    , providers: [InRtRt01Resolve, InRtRt01Service
        , InRtRt02Resolve, InRtRt02Service
        , InRtRt03Resolve, InRtRt03Service
        , InRtRt04Resolve, InRtRt04Service
        , InRtRt05Resolve, InRtRt05Service
    ]
})
export class InRtRoutingModule { }
