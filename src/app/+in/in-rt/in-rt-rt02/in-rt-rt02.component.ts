import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { InRtRt02Service } from './in-rt-rt02.service';
import { NgbModalOptions, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SelectItem } from "primeng/components/common/selectitem";

@Component({
    selector: 'app-in-rt-rt02'
    , templateUrl: './in-rt-rt02.component.html'
    , styleUrls: ['./in-rt-rt02.component.scss']
})
export class InRtRt02Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private modalOptions: NgbModalOptions = { backdrop: 'static', size: 'lg' }
    private recordsSearch: any[];
    private itemType: SelectItem[];
    private itemUnitType: SelectItem[];
    private itemWeigthType: SelectItem[];
    private customStatus: SelectItem[];
    private searchModal: boolean = false;

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal, private inRtRt05Service: InRtRt02Service) {

        this.form = fb.group({
            customId: fb.control(""),
            customStatus: fb.control("1", Validators.required),
            customName: fb.control("", Validators.required),
            customDesc: fb.control("", Validators.required),
            customContact: fb.control("", Validators.required),
            customTel: fb.control("", Validators.required),
            customFax: fb.control("", Validators.required),
            customEmail: fb.control("", Validators.required),
            customTaxCode: fb.control("", Validators.required),
            customWorkPlace: fb.control("", Validators.required),
            customCredit: fb.control("", Validators.required),

        });

        this.formSearchModal = fb.group({
            customId: fb.control("", Validators.required)
        });
    }

    ngOnInit() {

        this.customStatus = [];
        this.customStatus.push({ label: 'ใช้', value: 1 });
        this.customStatus.push({ label: 'ไม่ใช้', value: 2 });

        this.recordsSearch = [{
            'customId': 'CT001',
            'customName': 'คนสวย มากๆ',
            'customContact': 'คนหล่อ มากๆ',
            'customTel': '088888888',
            'customEmail': 'eieiei@gmail.com'
        }
            , {
            'customId': 'CT002',
            'customName': 'คนหล่อ มากๆ',
            'customContact': 'คนสวย มากๆ',
            'customTel': '0123456789',
            'customEmail': 'kikiki@gmail.com'
        }];
    }

    onClear() {
        this.form.reset();
        this.form.controls['customType'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "custom": this.form.value
        };
        this.inRtRt05Service.onSave('saveCustom', dataToSave).subscribe(
            data => {
                console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inRtRt05Service.onSearch('searchCustom', {}).subscribe(
            data => {
                this.recordsSearch = data.json().data.item
            },
            err => console.info(err));
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }

    private onRowSelect(event) {
        console.info(event);
    }
}