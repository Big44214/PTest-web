import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { InRtRt04Service } from './in-rt-rt04.service';
import { NgbModalOptions, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SelectItem } from "primeng/components/common/selectitem";

@Component({
    selector: 'app-in-rt-rt04'
    , templateUrl: './in-rt-rt04.component.html'
    , styleUrls: ['./in-rt-rt04.component.scss']
})
export class InRtRt04Component implements OnInit {

    private form: FormGroup;
    private formSearchModal: FormGroup;
    private modalOptions: NgbModalOptions = { backdrop: 'static', size: 'lg' }
    private recordsSearch: any[];
    private productcat: SelectItem[];
    private productcatStatus: SelectItem[]
    private productcatType: SelectItem[];
    private searchModal: boolean = false;

    constructor( @Inject(FormBuilder) fb: FormBuilder, private router: Router
        , private activatedRoute: ActivatedRoute
        , private modalService: NgbModal
        , private inRtRt05Service: InRtRt04Service) {

        this.form = fb.group({
            productcatId: fb.control(""),
            productcatName: fb.control("", Validators.required),
            productcatStatus: fb.control("1", Validators.required),
        });
        this.formSearchModal = fb.group({
            productcatId: fb.control("", Validators.required)
        });
    }

    ngOnInit() {
        this.productcatStatus = [];
        this.productcatStatus.push({ label: 'ใช้', value: 1 });
        this.productcatStatus.push({ label: 'ไม่ใช้', value: 2 });

        this.recordsSearch = [{
            'productcatId': 'Pro001'
            , 'productcatName': 'ทองคำแท่ง'
        }
            , {
            'productcatId': 'Pro002'
            , 'productcatName': 'ทองคำแท่ง'
        }];
    }

    onClear() {
        this.form.reset();
        this.form.controls['productcat'].setValue('');
    }

    onSave() {
        var dataToSave = {
            "productcat": this.form.value
        };
        this.inRtRt05Service.onSave('saveProductcat', dataToSave).subscribe(
            data => {
                console.info(data.json());
            },
            err => console.info(err));
    }

    onSearch() {
        this.inRtRt05Service.onSearch('searchProductcat', {}).subscribe(
            data => {
                this.recordsSearch = data.json().data.productcat
            },
            err => console.info(err));
    }

    private searchModalShow() {
        this.searchModal = true;
    }

    private searchModalHide() {
        this.searchModal = false;
    }

    private onRowSelect(event) {
        console.info(event);
    }

}