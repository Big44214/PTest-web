import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { GuardModule } from './shared/guard/guard.module';
import { ServiceModule } from './shared/services/service.module';

//For Shared Module.
import { SharedModule } from './shared/shared.module';

//For Core Module.
import { CoreModule } from './core/core.module';

//For Translate language.
import { Http } from '@angular/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//For boostrap4.
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//For router.
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';



// AoT requires an exported function for factories.
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

export const _IMPORTS = [
    BrowserModule, BrowserAnimationsModule, SharedModule, AppRoutingModule, CoreModule
    , GuardModule, ServiceModule, NgbModule.forRoot()
    , TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader
            , useFactory: HttpLoaderFactory
            , deps: [Http]
        }
    })
]

@NgModule({
    declarations: [ AppComponent ]
    , imports: _IMPORTS
    , bootstrap: [ AppComponent ]
})
export class AppModule { }
